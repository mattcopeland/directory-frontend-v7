export class AppUtils {
  /**
   * Get the unique values of a specified property from an array of objects
   *
   * @param arr
   * @param prop
   * @returns {string[]}
   */
  public static getUniquePropertyValuesFromObjectArray(arr, prop): string[] {
    return arr
      .map(data => data[prop])
      .filter((x, i, a) => x && a.indexOf(x) === i);
  }
}
