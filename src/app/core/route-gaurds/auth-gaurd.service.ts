import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivate,
  Router
} from '@angular/router';
import { UserService } from 'app/core/services/user.service';

@Injectable()
export class AuthGaurd implements CanActivate {
  constructor(private router: Router, private userService: UserService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean {
    return this.checkForLoggedIn(state.url);
  }

  checkForLoggedIn(url: string): boolean {
    if (this.userService.isAuthenticated) {
      return true;
    }
    this.userService.redirectUrl = url;
    this.router.navigate(['/pages/auth/login']);
    return false;
  }
}
