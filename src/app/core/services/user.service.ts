import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';

@Injectable()
export class UserService {
  TOKEN_KEY = 'token';
  redirectUrl: string;
  userAvatar: string;

  get token(): string {
    return localStorage.getItem(this.TOKEN_KEY);
  }

  get isAuthenticated(): boolean {
    return !!localStorage.getItem(this.TOKEN_KEY);
  }

  get isAdmin(): boolean {
    const jwtHelper = new JwtHelperService();
    const user = jwtHelper.decodeToken(this.token);
    return user.roles.indexOf('admin') >= 0;
  }

  get currentUser(): null | CurrentUser {
    if (!this.token) {
      return null;
    }
    // Use this if they have updated their avatar and not yet logged out and back in
    const avatar = localStorage.getItem('avatar');
    const jwtHelper = new JwtHelperService();
    const user = jwtHelper.decodeToken(this.token);
    return {
      id: user.sub,
      eid: user.eid,
      email: user.email,
      roles: user.roles,
      firstName: user.firstName,
      lastName: user.lastName,
      fullName: user.fullName,
      image: avatar || user.image,
      verified: user.verified
    };
  }

  constructor(private _router: Router) {}

  /**
   * Logout
   */
  logout(): void {
    localStorage.removeItem(this.TOKEN_KEY);
    localStorage.removeItem('avatar');
    this._router.navigate(['/']);
  }

  /**
   * Update user avatar
   *
   * @param {string} avatar
   */
  updateAvatar(avatar: string): void {
    localStorage.setItem('avatar', avatar);
  }
}

export class CurrentUser {
  id: string;
  eid: string;
  email: string;
  roles: string[];
  firstName: string;
  lastName: string;
  fullName: string;
  image: string;
  verified: boolean;
}
