import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from 'app/core/services/user.service';
import { AuthInterceptor } from 'app/core/interceptors/auth-interceptor.service';
import { AuthGaurd } from 'app/core/route-gaurds/auth-gaurd.service';
import { HttpCacheService } from 'app/core/services/http-cache.service';
import { CacheInterceptor } from 'app/core/interceptors/cache-interceptor.service';

@NgModule({
  imports: [CommonModule],
  providers: [
    UserService,
    AuthGaurd,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CacheInterceptor,
      multi: true
    },
    HttpCacheService
  ],
  declarations: [],
  exports: []
})
export class CoreModule {}
