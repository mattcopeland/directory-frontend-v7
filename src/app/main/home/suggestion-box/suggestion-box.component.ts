import { Component, OnInit, OnDestroy } from '@angular/core';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { UserService } from 'app/core/services/user.service';
import { HomeService } from 'app/main/home/services/home.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'suggestion-box',
  templateUrl: './suggestion-box.component.html',
  styleUrls: ['./suggestion-box.component.scss'],
  animations: fuseAnimations
})
export class SuggestionBoxComponent implements OnInit, OnDestroy {
  suggestionForm: FormGroup;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {MatSnackBar} _snackBar
   * @param {UserService} _userService
   * @param {HomeService} _homeService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _userService: UserService,
    private _homeService: HomeService,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) {
    this.suggestionForm = this._formBuilder.group({
      suggestion: ['']
    });
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {}

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Save the employee's fun fact
   */
  submitSuggestion(): void {
    if (!this.suggestionForm.value.suggestion) {
      return;
    }
    const suggestionData = {
      suggestion: this.suggestionForm.value.suggestion
    };
    if (this._userService.isAuthenticated) {
      suggestionData['eid'] = this._userService.currentUser.eid;
      suggestionData['submitterName'] =
        this._userService.currentUser.firstName +
        ' ' +
        this._userService.currentUser.lastName;
    } else {
      suggestionData['eid'] = null;
      suggestionData['submitterName'] = 'Anonymous';
    }
    this._homeService
      .submitSuggestion(suggestionData)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(data => {
        this.suggestionForm.reset();
        // Show a snackbar success
        this._snackBar.open('Your suggestion has been submitted.', null, {
          duration: 3000,
          panelClass: 'success'
        });
      });
  }
}
