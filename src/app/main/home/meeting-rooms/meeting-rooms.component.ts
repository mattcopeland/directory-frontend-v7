import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import * as _ from 'lodash';

import { OfficeService } from 'app/main/apps/office/services/office.service';
import { Room } from 'app/main/apps/office/models/room.model';

@Component({
  selector: 'meeting-rooms',
  templateUrl: './meeting-rooms.component.html',
  styleUrls: ['./meeting-rooms.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MeetingRoomsComponent implements OnInit, OnDestroy {
  rooms: Room[];
  sortedRooms: any[] = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  constructor(private _officeService: OfficeService) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    this._officeService
      .getRooms()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(rooms => {
        rooms = _.sortBy(rooms, ['name']);
        rooms.forEach(room => {
          if (!this.sortedRooms[room.officeCode + '-' + room.floor]) {
            this.sortedRooms[room.officeCode + '-' + room.floor] = [];
          }
          if (room.type.toLowerCase() === 'meeting') {
            this.sortedRooms[room.officeCode + '-' + room.floor].push(room);
          }
        });
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
