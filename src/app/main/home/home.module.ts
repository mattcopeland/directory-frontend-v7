import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  MatRippleModule,
  MatIconModule,
  MatButtonModule,
  MatInputModule,
  MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { SharedModule } from 'app/shared/shared.module';

import { HomeComponent } from './home.component';
import { AnniversariesComponent } from 'app/main/home/today/anniversaries/anniversaries.component';
import { EmployeeResolverService } from 'app/main/apps/employee/services/employee-resolver.service';
import { BirthdaysComponent } from 'app/main/home/today/birthdays/birthdays.component';
import { EventsResolverService } from 'app/main/home/services/events-resolver.service';
import { EventsComponent } from 'app/main/home/today/events/events.component';
import { TodayComponent } from 'app/main/home/today/today.component';
import { FeaturedEmployeeComponent } from './featured-employee/featured-employee.component';
import { QuicklinksComponent } from './quicklinks/quicklinks.component';
import { SuggestionBoxComponent } from './suggestion-box/suggestion-box.component';
import { MeetingRoomsComponent } from './meeting-rooms/meeting-rooms.component';
import { RoomResolverService } from '../apps/office/services/room-resolver.service';
import { PingPongVideoComponent } from './ping-pong-video/ping-pong-video.component';

const routes = [
  {
    path: '',
    component: HomeComponent,
    resolve: {
      employees: EmployeeResolverService,
      events: EventsResolverService,
      rooms: RoomResolverService
    }
  }
];

@NgModule({
  declarations: [
    HomeComponent,
    AnniversariesComponent,
    BirthdaysComponent,
    EventsComponent,
    TodayComponent,
    FeaturedEmployeeComponent,
    QuicklinksComponent,
    SuggestionBoxComponent,
    MeetingRoomsComponent,
    PingPongVideoComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatRippleModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatSnackBarModule,

    FuseSharedModule,
    FuseSidebarModule,

    SharedModule
  ],
  exports: [HomeComponent]
})
export class HomeModule {}
