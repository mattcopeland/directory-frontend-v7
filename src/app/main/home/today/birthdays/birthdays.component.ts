import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import * as moment from 'moment';
import * as _ from 'lodash';

import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { Router } from '@angular/router';

@Component({
  selector: 'birthdays',
  templateUrl: './birthdays.component.html',
  styleUrls: ['../anniversaries/anniversaries.component.scss']
})
export class BirthdaysComponent implements OnInit, OnDestroy {
  employees: Employee[];
  birthdayEmployees: Employee[];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   */
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => {
        this.employees = employees;
        this.filterEmployeesByAnniversaryToday(this.employees);
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filter all the employees to get just the ones with an anniversary today
   *
   * @param {Array} employees
   */
  filterEmployeesByAnniversaryToday(employees: Employee[]): void {
    const currentDay = moment().date();
    const currentMonth = moment().month();
    this.birthdayEmployees = _.sortBy(
      employees.filter(employee => {
        if (
          employee.hireDate &&
          currentMonth === moment(employee.birthdate).month() &&
          currentDay === moment(employee.birthdate).date()
        ) {
          return employee;
        }
      }),
      'birthday'
    );
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * View the employee detail of the selected employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee): void {
    this._employeeService.changeSelectedEmployee(employee);
    // Add the eid to the url for refreshing
    this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }
}
