import { Component, OnInit } from '@angular/core';

import * as moment from 'moment';

@Component({
  selector: 'today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.scss']
})
export class TodayComponent implements OnInit {
  date: Date;
  ordinalIndicator: string;
  /**
   * Constructor
   *
   */
  constructor() {
    // Set the defaults
    this.date = new Date();

    // Figure out today's month ordinal
    switch (moment().date()) {
      case 1:
      case 21:
      case 31:
        this.ordinalIndicator = 'st';
        break;
      case 2:
      case 22:
        this.ordinalIndicator = 'nd';
        break;
      case 3:
      case 23:
        this.ordinalIndicator = 'rd';
        break;
      default:
        this.ordinalIndicator = 'th';
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {}
}
