import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import * as moment from 'moment';
import * as _ from 'lodash';

import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { Router } from '@angular/router';

@Component({
  selector: 'featured-employee',
  templateUrl: './featured-employee.component.html',
  styleUrls: ['./featured-employee.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FeaturedEmployeeComponent implements OnInit, OnDestroy {
  employees: Employee[];
  featuredEmployee: any;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   */
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => {
        // Remove excluded employees from search results
        this.employees = employees.filter(employee => {
          return !employee.exclude;
        });
        this._employeeService
          .getFeaturedEmployee()
          .pipe(takeUntil(this._unsubscribeAll))
          .subscribe(featuredEmployee => {
            this.featuredEmployee = this.selectFeaturedEmployee(
              featuredEmployee['eid']
            );
            this.calculateEmploymentLength(this.featuredEmployee);
            this.getEmployeeLocation(this.featuredEmployee);
          });
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Find the featured employee from the list of all employees
   * using their eid from the endpoint call
   *
   * @param {number} eid
   */
  selectFeaturedEmployee(eid: number): Employee {
    return this.employees.find(employee => {
      return employee.eid === eid;
    });
  }

  /**
   * Calculate the length of service for this employee in readable terms
   *
   * @param {Employee} employee
   */
  calculateEmploymentLength(employee): any {
    if (!employee.hireDate) {
      return employee;
    }
    const employmentMonths = moment().diff(employee.hireDate, 'months', true);

    if (employmentMonths < 12) {
      employee.employmentLength = Math.floor(employmentMonths);
      if (employmentMonths < 2) {
        employee.employmentLength = '<div>1</div><div>Month at Liazon</div>';
      } else {
        employee.employmentLength =
          '<div>' +
          employee.employmentLength +
          '</div><div>Months at Liazon</div>';
      }
    } else {
      if (employmentMonths % 12 < 4) {
        employee.employmentLength =
          '<div>' +
          Math.floor(employmentMonths / 12) +
          '</div><div>Years at Liazon</div>';
      } else if (employmentMonths % 12 < 10) {
        employee.employmentLength =
          '<div>' +
          Math.floor(employmentMonths / 12) +
          '.5</div><div>Years at Liazon</div>';
      } else if (employmentMonths % 12 < 12) {
        employee.employmentLength =
          '<div>' +
          Math.ceil(employmentMonths / 12) +
          '</div><div>Years at Liazon</div>';
      }
    }
    return employee;
  }

  /**
   * GEt the location for this employee in readable terms
   *
   * @param {Employee} employee
   */
  getEmployeeLocation(employee): any {
    if (!employee.officeCode || employee.officeCode === 'oth') {
      employee.location = '<div>Remote</div><div>Worker</div>';
    } else {
      employee.location =
        '<div>' +
        employee.floor +
        '<sup>th</sup> floor</div><div>' +
        employee.office.city +
        ' Office</div>';
    }
    return employee;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * View the employee detail of the selected employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee): void {
    this._employeeService.changeSelectedEmployee(employee);
    // Add the eid to the url for refreshing
    this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }
}
