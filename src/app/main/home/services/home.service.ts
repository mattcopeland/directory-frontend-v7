import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  events: any;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get today's events
   *
   */
  getEventsToday(): Observable<any[]> {
    return this._httpClient.get<any[]>('/api/calendar/events/today').pipe(
      tap(events => {
        this.events = events;
      })
    );
  }

  /**
   * Submit an employee's suggestion
   *
   * @param {any} suggestionData
   */
  submitSuggestion(suggestionData: any): Observable<any> {
    return this._httpClient.post('/api/suggestion', suggestionData);
  }
}
