import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Observable } from 'rxjs';

import { HomeService } from './home.service';

@Injectable({
  providedIn: 'root'
})
export class EventsResolverService implements Resolve<any[]> {
  constructor(private _homeService: HomeService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any[]> {
    return this._homeService.getEventsToday();
  }
}
