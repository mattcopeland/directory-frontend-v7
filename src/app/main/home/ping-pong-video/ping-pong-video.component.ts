import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'ping-pong-video',
  templateUrl: './ping-pong-video.component.html',
  styleUrls: ['./ping-pong-video.component.scss']
})
export class PingPongVideoComponent implements OnInit {
  @ViewChild('pingpong')
  pingpongEl: ElementRef;
  /**
   * Constructor
   *
   */
  constructor() {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.pingpongEl.nativeElement.addEventListener(
      'loadedmetadata',
      function(): void {
        // Start video with my smash
        this.currentTime = 23;
      },
      false
    );
  }
}
