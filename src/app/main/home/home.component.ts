import { Component, OnInit } from '@angular/core';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { fuseAnimations } from '@fuse/animations';

import * as moment from 'moment';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: fuseAnimations
})
export class HomeComponent implements OnInit {
  pingpong: Boolean = false;

  /**
   * Constructor
   *
   * @param {FuseSidebarService} _fuseSidebarService
   */
  constructor(private _fuseSidebarService: FuseSidebarService) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    const magicNumber = this.getRandomInt(0, 60);

    if (+moment().format('m') === magicNumber) {
      this.pingpong = true;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Toggle sidebar
   *
   * @param {string} name
   */
  toggleSidebar(name: string): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }

  private getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
}
