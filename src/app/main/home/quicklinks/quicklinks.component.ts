import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { UserService } from 'app/core/services/user.service';

@Component({
  selector: 'quicklinks',
  templateUrl: './quicklinks.component.html',
  styleUrls: ['./quicklinks.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class QuicklinksComponent implements OnInit {
  constructor(public userService: UserService) {}

  ngOnInit(): void {}
}
