import { NgModule } from '@angular/core';

import { LoginModule } from 'app/main/pages/authentication/login/login.module';
import { RegisterModule } from 'app/main/pages/authentication/register/register.module';
import { MailConfirmModule } from 'app/main/pages/authentication/mail-confirm/mail-confirm.module';
import { ForgotPasswordModule } from 'app/main/pages/authentication/forgot-password/forgot-password.module';
import { ResetPasswordModule } from 'app/main/pages/authentication/reset-password/reset-password.module';

import { AuthService } from 'app/main/pages/authentication/auth.service';

@NgModule({
  imports: [
    // Authentication
    LoginModule,
    RegisterModule,
    MailConfirmModule,
    ForgotPasswordModule,
    ResetPasswordModule
  ],
  providers: [AuthService]
})
export class PagesModule {}
