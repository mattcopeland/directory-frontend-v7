import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) {}

  registerUser(registerData: any): Observable<any> {
    return this.http.post('/api/auth/register/', registerData);
  }

  loginUser(loginData: any): Observable<any> {
    return this.http.post('/api/auth/login/', loginData);
  }

  verifyUser(userId: string, verificationToken: string): Observable<any> {
    const params = new HttpParams({
      fromObject: {
        userId: userId,
        verificationToken: verificationToken
      }
    });
    return this.http.get('/api/auth/verifyUser/', { params: params });
  }

  generatePasswordResetEmail(email: string): Observable<any> {
    const params = new HttpParams({
      fromObject: {
        email: email
      }
    });
    return this.http.get('/api/auth/generatePasswordResetEmail/', {
      params: params
    });
  }

  resetPassword(formData): Observable<any> {
    return this.http.post('/api/auth/passwordReset/', formData);
  }
}
