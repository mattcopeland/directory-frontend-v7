import { Component, ViewEncapsulation, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import { AuthService } from 'app/main/pages/authentication/auth.service';
import { UserService } from 'app/core/services/user.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
  selector: 'mail-confirm',
  templateUrl: './mail-confirm.component.html',
  styleUrls: ['./mail-confirm.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class MailConfirmComponent implements OnInit, OnDestroy {
  verified = false;
  verifying = false;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseConfigService} _fuseConfigService
   * @param {ActivatedRoute} _route
   * @param {AuthService} _authService
   * @param {UserService} _userService
   */
  constructor(
    private _fuseConfigService: FuseConfigService,
    private _route: ActivatedRoute,
    private _authService: AuthService,
    private _userService: UserService
  ) {
    // Configure the layout
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    const userId = this._route.snapshot.params['u'];
    const verificationToken = this._route.snapshot.params['t'];

    if (userId && verificationToken) {
      this.verifying = true;
      this._authService
        .verifyUser(userId, verificationToken)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
          data => {
            this.verified = true;
          },
          (error: any) => {
            this.verified = false;
          }
        );
    }
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
