import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatIconModule,
  MatDialogModule,
  MatToolbarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { RegisterComponent } from 'app/main/pages/authentication/register/register.component';
import { TosDialogComponent } from 'app/main/pages/authentication/register/tos-dialog/tos-dialog.component';

const routes = [
  {
    path: 'auth/register',
    component: RegisterComponent
  }
];

@NgModule({
  declarations: [RegisterComponent, TosDialogComponent],
  imports: [
    RouterModule.forChild(routes),

    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatDialogModule,
    MatToolbarModule,

    FuseSharedModule
  ],
  entryComponents: [TosDialogComponent]
})
export class RegisterModule {}
