import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';

@Component({
  selector: 'tos-dialog',
  templateUrl: './tos-dialog.component.html',
  styleUrls: ['./tos-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class TosDialogComponent {
  dialogTitle: string;

  /**
   * Constructor
   *
   * @param {MatDialogRef<TosDialogComponent>} matDialogRef
   * @param _data
   * @param {Router} _router
   */
  constructor(
    public matDialogRef: MatDialogRef<TosDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Accept the terms of service
   *
   */
  accept(): void {
    this.matDialogRef.close();
  }
}
