export class Job {
  department: string;
  jobTitle: string;

  /**
   * Constructor
   *
   * @param job
   */
  constructor(job) {
    {
      this.department = job.department;
      this.jobTitle = job.jobTitle;
    }
  }
}
