import { Component, OnInit, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { UserService } from 'app/core/services/user.service';
import { OfficeService } from 'app/main/apps/office/services/office.service';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { Desk } from 'app/main/apps/office/models/desk.model';
import { Employee } from 'app/main/apps/employee/models/employee.model';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['../../../office/map/map.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MapComponent implements OnInit, OnDestroy {
  desks: Desk[];
  filteredDesks: Desk[];
  employees: Employee[];
  eid: number;
  floor: number;
  officeCode: string;
  selectedEmployee: Employee;

  get employeesMappedToDesks(): Desk[] {
    return this._officeService.employeesMappedToDesks;
  }

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {OfficeService} _officeService
   * @param {EmployeeService} _employeeService
   */
  constructor(
    private _officeService: OfficeService,
    private _employeeService: EmployeeService,
    private _userService: UserService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _adminDataAccessService: AdminDataAccessService,
    private _snackBar: MatSnackBar
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    this.desks =
      this.employeesMappedToDesks || this._route.snapshot.data['desks'];
    this.eid = +this._route.snapshot.params['eid'];
    this.floor = +this._route.snapshot.params['floor'];
    this.officeCode = this._route.snapshot.params['officeCode'];

    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => {
        this.employees = employees;
        this.mapOccupantToDesks(this.desks);
      });

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filter the complete list of desks to only the desks for this location
   */
  filterDesks(): void {
    this.filteredDesks = this.desks.filter(
      desk => desk.floor === +this.floor && desk.officeCode === this.officeCode
    );
  }

  /**
   * Map each employee to the desk that occupy
   *
   * @param {Desk[]} desks
   */
  mapOccupantToDesks(desks: Desk[]): void {
    if (!this.employeesMappedToDesks) {
      this.desks.forEach(desk => {
        desk.occupant = this.employees.find(employee => {
          return (
            desk.seat === employee.seat &&
            desk.officeCode === employee.officeCode
          );
        });
      });
    }
    this._officeService.employeesMappedToDesks = this.desks;
    this.filterDesks();
  }

  /**
   * Update the occupancies of the desks that were changed
   *
   * @param {number} newDeskIndex
   * @param {number} oldDeskIndex
   */
  updateOccupantMappedDesks(newDeskIndex: number, oldDeskIndex: number): void {
    // Transferring from a different seat
    if (oldDeskIndex > -1) {
      this.desks[newDeskIndex].occupant = this.desks[oldDeskIndex].occupant;
      this.desks[oldDeskIndex].occupant = null;
    } else {
      // They didn't have a seat before
      this.desks[newDeskIndex].occupant = this.selectedEmployee;
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Assign a desk to an employee
   *
   * @param {Desk} desk
   */
  assignDesk(desk: Desk): void {
    if (desk.occupant) {
      const msg = `${desk.occupant.displayName} is already sitting there.`;
      this._snackBar.open(msg, null, {
        duration: 3000,
        panelClass: 'error'
      });
      return;
    }
    const deskData = {
      _id: this._userService.currentUser.id,
      seat: desk.seat,
      eid: this.eid,
      floor: this.floor,
      officeCode: this.officeCode
    };

    // Get the index of the desk they are moving from
    const oldDeskIndex = this.desks.findIndex(d => {
      return d.occupant && d.occupant.eid === this.eid;
    });

    // Get the index of the desk they are moving to
    const newDeskIndex = this.desks.findIndex(d => {
      return (
        d.seat === desk.seat &&
        d.floor === this.floor &&
        d.officeCode === this.officeCode
      );
    });

    this._adminDataAccessService
      .assignDesk(deskData)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        data => {
          // Move the occupant in the cached list of desks
          this.updateOccupantMappedDesks(newDeskIndex, oldDeskIndex);
          // Show a snackbar success
          this._snackBar.open('Employee desk updated successfully', null, {
            duration: 3000,
            panelClass: 'success'
          });
          this._router.navigate(['/apps/employee', { eid: this.eid }]);
        },
        // Show a snackbar error
        (response: any) => {
          this._snackBar.open(response.error.message, null, {
            duration: 3000,
            panelClass: 'error'
          });
        }
      );
  }
}
