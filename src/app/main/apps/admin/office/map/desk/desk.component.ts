import { Component, OnInit, Input } from '@angular/core';
import { Desk } from 'app/main/apps/office/models/desk.model';

@Component({
  selector: 'desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.scss']
})
export class DeskComponent implements OnInit {
  @Input()
  desk: Desk;
  constructor() {}

  ngOnInit(): void {}
}
