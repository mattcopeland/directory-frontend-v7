import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { Office } from 'app/main/apps/office/models/office.model';
import { UserService } from 'app/core/services/user.service';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';

@Component({
  selector: 'office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OfficeComponent implements OnInit, OnDestroy {
  offices: Office[];
  eid: number;
  selectedEmployee: Employee;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {ActivatedRoute} _route
   * @param {Router} _router
   */
  constructor(
    private _route: ActivatedRoute,
    private _router: Router,
    private _userService: UserService,
    private _employeeService: EmployeeService,
    private _adminDataAccessService: AdminDataAccessService,
    private _snackBar: MatSnackBar
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    this.offices = this._route.snapshot.data['offices'];
    this.eid = +this._route.snapshot.params['eid'];

    // If there is no eid on the url then try to use the current user's eid
    // Otherwise use the eid to set the selected user
    if (!this.eid) {
      this.eid = +this._userService.currentUser.eid;
      this._router.navigate([
        '/apps/admin/office',
        { eid: this._userService.currentUser.eid }
      ]);
    } else {
      this.selectedEmployee = this._employeeService.selectEmployeeByEid(
        this.eid
      );
    }

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
        if (!this.selectedEmployee) {
          this._router.navigate([
            '/apps/admin/employee/admin',
            { eid: this.eid }
          ]);
        }
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Select an office locaiton
   *
   * @param {number} eid
   * @param {number} floor
   * @param {string} officeCode
   */
  selectLocation(eid: number, floor: number, officeCode: string): void {
    this._router.navigate([
      '/apps/admin/office/map',
      { eid: eid, floor: floor, officeCode: officeCode }
    ]);
  }

  makeRemote(eid: number): void {
    this._adminDataAccessService
      .makeRemote({ eid: eid })
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        data => {
          // Move the occupant to the cached list of desks
          this.selectedEmployee.office = null;
          this.selectedEmployee.officeCode = 'oth';
          this.selectedEmployee.officeCitySate = null;
          this.selectedEmployee.floor = null;
          this.selectedEmployee.seat = null;
          // Show a snackbar success
          this._snackBar.open('Employee desk updated successfully', null, {
            duration: 3000,
            panelClass: 'success'
          });
          this._router.navigate(['/apps/employee', { eid: this.eid }]);
        },
        // Show a snackbar error
        (response: any) => {
          this._snackBar.open(response.error.message, null, {
            duration: 3000,
            panelClass: 'error'
          });
        }
      );
  }
}
