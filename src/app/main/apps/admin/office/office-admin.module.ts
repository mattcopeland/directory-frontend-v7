import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import {
  MatIconModule,
  MatButtonModule,
  MatSnackBarModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { OfficeResolverService } from 'app/main/apps/office/services/office-resolver.service';
import { DeskResolverService } from 'app/main/apps/office/services/desk-resolver.service';
import { OfficeComponent } from 'app/main/apps/admin/office/office.component';
import { DeskComponent } from 'app/main/apps/admin/office/map/desk/desk.component';
import { AuthGaurd } from 'app/core/route-gaurds/auth-gaurd.service';
import { MapComponent } from 'app/main/apps/admin/office/map/map.component';
import { EmployeeResolverService } from 'app/main/apps/employee/services/employee-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: OfficeComponent,
    canActivate: [AuthGaurd],
    resolve: {
      offices: OfficeResolverService,
      employees: EmployeeResolverService
    }
  },
  {
    path: 'map',
    component: MapComponent,
    canActivate: [AuthGaurd],
    resolve: { desks: DeskResolverService }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatButtonModule,
    MatSnackBarModule,

    FuseSharedModule
  ],
  declarations: [DeskComponent, OfficeComponent, MapComponent]
})
export class OfficeAdminModule {}
