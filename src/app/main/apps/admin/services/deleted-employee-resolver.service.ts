import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Observable } from 'rxjs';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';

@Injectable({
  providedIn: 'root'
})
export class DeletedEmployeeResolverService implements Resolve<Employee[]> {
  constructor(private _adminDataAccessService: AdminDataAccessService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Employee[]> {
    return this._adminDataAccessService.getDeletedEmployees();
  }
}
