import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Observable } from 'rxjs';

import { Job } from 'app/main/apps/admin/models/job.model';
import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';

@Injectable({
  providedIn: 'root'
})
export class JobResolverService implements Resolve<Job[]> {
  constructor(private _adminDataAccessService: AdminDataAccessService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Job[]> {
    return this._adminDataAccessService.getJobs();
  }
}
