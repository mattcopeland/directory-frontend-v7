import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/internal/operators';

import { Job } from 'app/main/apps/admin/models/job.model';
import { Employee } from '../../employee/models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class AdminDataAccessService {
  jobs: Job[];
  deletedEmployees: Employee[];

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get jobs
   *
   */
  getJobs(): Observable<Job[]> {
    return this._httpClient
      .get<Job[]>('/api/jobs')
      .pipe(tap(data => (this.jobs = data)));
  }

  /**
   * Get deleted employees
   *
   */
  getDeletedEmployees(): Observable<Employee[]> {
    return this._httpClient
      .get<Employee[]>('/api/employees/deleted')
      .pipe(tap(data => (this.deletedEmployees = data)));
  }

  /**
   * Updates an existing employee
   *
   * @param {Employee} employeeData
   */
  updateEmployee(employeeData: Employee): Observable<any> {
    return this._httpClient.put('/api/employee/update', employeeData);
  }

  /**
   * Creates a new employee
   *
   * @param {Employee} employeeData
   */
  createEmployee(employeeData: Employee): Observable<any> {
    return this._httpClient.post('/api/employee/create', employeeData);
  }

  /**
   * Deletes an existing employee
   *
   * @param {Employee} employeeData
   */
  deleteEmployee(employeeData: Employee): Observable<any> {
    return this._httpClient.put('/api/employee/delete', employeeData);
  }

  /**
   * Upload Employee avatar
   *
   * @param {any} avatarData
   */
  uploadAvatar(avatarData: any): Observable<any> {
    return this._httpClient.put('/api/employee/avatar', avatarData);
  }

  /**
   * Assign a desk to an employee
   *
   * @param {any} avatarData
   */
  assignDesk(deskData: any): Observable<any> {
    return this._httpClient.put('/api/employee/desk', deskData);
  }

  /**
   * Assign an employee as a remote worker
   *
   * @param {any} eid
   */
  makeRemote(eid: any): Observable<any> {
    return this._httpClient.put('/api/employee/remote', eid);
  }

  /**
   * Save an employee's fun fact
   *
   * @param {any} funFactData
   */
  updateFunFact(funFactData: any): Observable<any> {
    return this._httpClient.post('/api/employee/funFact', funFactData);
  }
}
