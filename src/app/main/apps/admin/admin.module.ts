import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
  {
    path: 'employee',
    loadChildren: './employee/employee-admin.module#EmployeeAdminModule'
  },
  {
    path: 'office',
    loadChildren: './office/office-admin.module#OfficeAdminModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), FuseSharedModule]
})
export class AdminModule {}
