import {
  Component,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import { ImageCroppedEvent } from 'ngx-image-cropper/src/image-cropper.component';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { UserService } from 'app/core/services/user.service';

@Component({
  selector: 'employee-avatar',
  templateUrl: './employee-avatar.component.html',
  styleUrls: ['./employee-avatar.component.scss'],
  animations: fuseAnimations
})
export class EmployeeAvatarComponent implements OnInit, OnDestroy {
  imageChangedEvent: any = '';
  croppedImage: any = '';
  cropperReady = false;
  croppingImage = false;

  selectedEmployee: Employee;

  @ViewChild('fileInput')
  fileInputRef: ElementRef;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {AdminDataAccessService} _adminDataAccessService
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   */
  constructor(
    private _adminDataAccessService: AdminDataAccessService,
    private _employeeService: EmployeeService,
    private _router: Router,
    private _userService: UserService,
    private _snackBar: MatSnackBar
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public Methods
  // -----------------------------------------------------------------------------------------------------

  fileChangeEvent(event: any): void {
    this.imageChangedEvent = event;
    this.croppingImage = true;
  }
  imageCropped(event: ImageCroppedEvent): void {
    this.croppedImage = event.base64;
  }
  imageLoaded(): void {
    this.cropperReady = true;
  }
  loadImageFailed(): void {
    console.log('Load failed');
  }

  /**
   * Upload the updated avatar
   */
  uploadAvatar(): void {
    // 10MB limit
    if (this.fileInputRef.nativeElement.files[0].size > 10000000) {
      this._snackBar.open('File is too large.', null, {
        duration: 3000,
        panelClass: 'error'
      });
    } else {
      const updatedImage = {
        avatar: this.croppedImage,
        eid: this.selectedEmployee.eid
      };

      this._adminDataAccessService
        .uploadAvatar(updatedImage)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
          data => {
            // Set the new avatar for user if the user is editing their own profile
            if (+this._userService.currentUser.eid === updatedImage.eid) {
              this._userService.updateAvatar(data.fileName);
            }
            // Set the new avatar for the employee
            this.selectedEmployee.image = data.fileName;
            this._employeeService.changeSelectedEmployee(this.selectedEmployee);
            this._router.navigate([
              '/apps/employee',
              { eid: this.selectedEmployee.eid }
            ]);
          },
          // Show a snackbar error
          (response: any) => {
            this._snackBar.open(response.error.message, null, {
              duration: 3000,
              panelClass: 'error'
            });
          }
        );
    }
  }
}
