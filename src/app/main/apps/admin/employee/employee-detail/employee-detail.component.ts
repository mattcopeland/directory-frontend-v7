import {
  Component,
  OnInit,
  ViewEncapsulation,
  OnDestroy,
  Input,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import { takeUntil, startWith, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import * as _ from 'lodash';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { AppUtils } from 'app/core/utils';
import { Job } from 'app/main/apps/admin/models/job.model';
import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { UserService } from 'app/core/services/user.service';

@Component({
  selector: 'employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class EmployeeDetailComponent implements OnInit, OnDestroy {
  employeeForm: FormGroup;
  selectedEmployee: Employee | null;
  manager: Employee | null;
  directReports: Employee[] | null;
  departments: string[];
  jobTitles: string[];
  jobClassifications = ['Peak', 'Lead', 'Senior', 'Associate', 'Intern'];
  filteredEmployees: Observable<Employee[]>;
  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
  adminUser: boolean;

  @Input()
  jobs: Job[];

  @ViewChild(FusePerfectScrollbarDirective)
  directiveScroll: FusePerfectScrollbarDirective;

  // Private
  private _unsubscribeAll: Subject<any>;
  private employees: Employee[];
  // filter for manager autocomplete
  private _filter(employeeName: string): Employee[] {
    const filterValue = employeeName.toLowerCase();
    return this.employees.filter(
      e => e.displayName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  /**
   * Constructor
   *
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   * @param {FormBuilder} _formBuilder
   * @param {MatSnackBar} _snackBar
   * @param {AdminDataAccessService} _adminDataAccessService
   */
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _adminDataAccessService: AdminDataAccessService,
    private _userService: UserService,
    private _matDialog: MatDialog
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.initializeEmployeeForm();

    this.adminUser = this._userService.isAdmin;

    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => {
        this.employees = employees;
      });

    this.departments = AppUtils.getUniquePropertyValuesFromObjectArray(
      this.jobs,
      'department'
    ).sort();
    this.jobTitles = AppUtils.getUniquePropertyValuesFromObjectArray(
      this.jobs,
      'jobTitle'
    ).sort();

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
        if (this.selectedEmployee && this.employees) {
          this.filterJobTitlesByDepartment(this.selectedEmployee.department);
          this.employeeForm.patchValue({
            eid: selectedEmployee.eid,
            nickname: selectedEmployee.nickname,
            email: selectedEmployee.email,
            firstName: selectedEmployee.firstName,
            lastName: selectedEmployee.lastName,
            phone: selectedEmployee.phone,
            ext: selectedEmployee.ext,
            team: selectedEmployee.team,
            department: selectedEmployee.department,
            jobTitle: selectedEmployee.jobTitle,
            jobClassification: selectedEmployee.jobClassification,
            hireDate: selectedEmployee.hireDate,
            birthdate: selectedEmployee.birthdate,
            manager: this.getManager(selectedEmployee)
          });
          setTimeout(() => {
            this.scrollToTop();
          });
        } else {
          this.initializeEmployeeForm();
        }
      });

    // Auto complete for manager
    this.filteredEmployees = this.employeeForm.controls[
      'manager'
    ].valueChanges.pipe(
      startWith<string | Employee>(''),
      map(value => (typeof value === 'string' ? value : value.displayName)),
      map(name => (name ? this._filter(name) : this.employees.slice()))
    );
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  initializeEmployeeForm(): void {
    this.employeeForm = this._formBuilder.group({
      eid: [
        { value: '', disabled: !this._userService.isAdmin },
        [Validators.required]
      ],
      nickname: [''],
      email: ['', [Validators.required, Validators.email]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      phone: [''],
      ext: [''],
      team: [{ value: '', disabled: !this._userService.isAdmin }],
      department: [{ value: '', disabled: !this._userService.isAdmin }],
      jobTitle: [{ value: '', disabled: !this._userService.isAdmin }],
      jobClassification: [{ value: '', disabled: !this._userService.isAdmin }],
      hireDate: [{ value: '', disabled: !this._userService.isAdmin }],
      birthdate: [{ value: '', disabled: !this._userService.isAdmin }],
      manager: [{ value: '', disabled: !this._userService.isAdmin }]
    });

    // Auto complete for manager
    this.filteredEmployees = this.employeeForm.controls[
      'manager'
    ].valueChanges.pipe(
      startWith<string | Employee>(''),
      map(value => (typeof value === 'string' ? value : value.displayName)),
      map(name => (name ? this._filter(name) : this.employees.slice()))
    );
  }

  /**
   * Get the manager for an employee
   *
   * @param {Employee} employee
   */
  private getManager(employee: Employee): Employee | null {
    if (!employee) {
      return null;
    }
    return this.employees.find(e => {
      return e.eid === employee.mid;
    });
  }

  /**
   * Get all the job tiltes in a specified department
   *
   * @param {string} department
   */
  filterJobTitlesByDepartment(department: string): void {
    this.jobTitles = [];
    const departmentJobs = this.jobs.filter(job => {
      return job.department.toLowerCase() === department.toLowerCase();
    });
    departmentJobs.forEach(job => {
      this.jobTitles.push(job.jobTitle);
    });
  }

  /**
   * Used to display the manager's display name in the auto complete box
   *
   * @param {Employee} employee
   */
  displayManagerName(employee?: Employee): string | undefined {
    return employee ? employee.displayName : undefined;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Save the employee updates
   */
  saveEmployee(): void {
    const employeeData = this.employeeForm.value;
    employeeData.displayName =
      (this.employeeForm.value.nickname || this.employeeForm.value.firstName) +
      ' ' +
      this.employeeForm.value.lastName;
    // No upper case in email
    employeeData.email = employeeData.email.toLowerCase();
    // Only admins will be allowed to upadte the manager
    if (this.employeeForm.value.manager) {
      employeeData.mid = this.employeeForm.value.manager.eid;
      delete employeeData.manager;
    }

    // If this is an existing employee update it
    if (this.selectedEmployee) {
      employeeData._id = this.selectedEmployee._id;
      // Remove all non-numeric characters from phone number
      if (employeeData.phone && isNaN(employeeData.phone)) {
        employeeData.phone = employeeData.phone.replace(/\D/g, '');
      }
      const employeeIndex = this.employees.findIndex(e => {
        return e.eid === this.selectedEmployee.eid;
      });

      // Only admin user's can update the job classification
      // and they might want to set it back to null
      if (this.adminUser) {
        employeeData.jobClassification = employeeData.jobClassification || null;
      }

      // Update this employee in the list of all employees
      const existingEmployee = Object.assign(
        this.employees[employeeIndex],
        employeeData
      );
      this.employees.splice(employeeIndex, 1, existingEmployee);
      this._employeeService.changeSelectedEmployee(existingEmployee);

      this._adminDataAccessService
        .updateEmployee(employeeData)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
          data => {
            // Show a snackbar success
            this._snackBar.open('Employee updated successfully', null, {
              duration: 3000,
              panelClass: 'success'
            });
            this._router.navigate([
              '/apps/employee',
              { eid: employeeData.eid }
            ]);
          },
          // Show a snackbar error
          (response: any) => {
            this._snackBar.open(response.error.message, null, {
              duration: 3000,
              panelClass: 'error'
            });
          }
        );
    } else {
      // Create a new employee
      employeeData.image = 'profile.png';
      this._adminDataAccessService
        .createEmployee(employeeData)
        .pipe(takeUntil(this._unsubscribeAll))
        .subscribe(
          (response: any) => {
            this._employeeService.changeSelectedEmployee(employeeData);
            // Show a snackbar success
            this._snackBar.open(
              employeeData.displayName + ' created successfully',
              null,
              {
                duration: 3000,
                panelClass: 'success'
              }
            );
            this._router.navigate([
              '/apps/employee',
              { eid: employeeData.eid }
            ]);
          },
          // Show a snackbar error
          (response: any) => {
            this._snackBar.open(response.error.message, null, {
              duration: 3000,
              panelClass: 'error'
            });
          }
        );
    }
  }

  /**
   * View the employee detail of the selected employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee): void {
    if (employee.mid) {
      this._employeeService.changeSelectedEmployee(employee);
      // Add the eid to the url for refreshing
      this._router.navigate([
        '/apps/admin/employee/admin',
        { eid: employee.eid }
      ]);
      setTimeout(() => {
        this.scrollToTop();
      });
    }
  }

  /**
   * Delete an existing employee
   *
   * @param {Employee} employee
   */
  deleteEmployee(employee: Employee): void {
    this.confirmDialogRef = this._matDialog.open(FuseConfirmDialogComponent, {
      disableClose: false
    });

    this.confirmDialogRef.componentInstance.confirmMessage =
      'Are you sure you want to delete?';

    this.confirmDialogRef
      .afterClosed()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(result => {
        if (result) {
          employee.deleted = true;
          this._adminDataAccessService
            .deleteEmployee(employee)
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(
              (response: any) => {
                // Reassign all this person's direct reports to their manager
                const directReports = this.employees.filter(e => {
                  return e.mid === employee.eid;
                });
                // Update all the direct reports in the database
                directReports.forEach(directReport => {
                  directReport.mid = employee.mid;
                  this._adminDataAccessService
                    .updateEmployee(directReport)
                    .pipe(takeUntil(this._unsubscribeAll))
                    .subscribe(data => {
                      // Update all the direct reports in the cached list of employees
                      const e = this.employees.find(dr => {
                        return dr.eid === directReport.eid;
                      });
                      e.mid = directReport.mid;
                    });
                });
                // Remove this employee as the selected employee
                this._employeeService.changeSelectedEmployee(null);
                // Remove this employee from the list of employees
                const employeeIndex = this.employees.findIndex(e => {
                  return e.eid === employee.eid;
                });
                this.employees.splice(employeeIndex, 1);
                // Show a snackbar success
                this._snackBar.open(employee.displayName + ' deleted', null, {
                  duration: 3000,
                  panelClass: 'success'
                });
                this._router.navigate(['/apps/employee']);
              },
              // Show a snackbar error
              (response: any) => {
                this._snackBar.open(response.error.message, null, {
                  duration: 3000,
                  panelClass: 'error'
                });
              }
            );
        }
        this.confirmDialogRef = null;
      });
  }

  /**
   * Scroll to the top
   *
   * @param {number} speed
   */
  scrollToTop(speed?: number): void {
    speed = speed || 400;
    if (this.directiveScroll) {
      this.directiveScroll.update();

      setTimeout(() => {
        this.directiveScroll.scrollToTop(0, speed);
      });
    }
  }
}
