import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import {
  MatIconModule,
  MatSelectModule,
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatSnackBarModule,
  MatDialogModule,
  MatAutocompleteModule
} from '@angular/material';
import {
  MatDatepickerModule,
  MatMomentDateModule
} from '@coachcare/datepicker';
import { ImageCropperModule } from 'ngx-image-cropper';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseConfirmDialogModule } from '@fuse/components';

import { SharedModule } from 'app/shared/shared.module';

import { EmployeeResolverService } from 'app/main/apps/employee/services/employee-resolver.service';
import { EmployeeComponent } from './employee.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { JobResolverService } from '../services/job-resolver.service';
import { AuthGaurd } from 'app/core/route-gaurds/auth-gaurd.service';
import { EmployeeAvatarComponent } from './employee-avatar/employee-avatar.component';
import { EmployeeDeskComponent } from './employee-desk/employee-desk.component';
import { RestoreEmployeeComponent } from './restore-employee/restore-employee.component';
import { RestoreEmployeeDetailComponent } from './restore-employee/restore-employee-detail/restore-employee-detail.component';
import { RestoreEmployeeListComponent } from './restore-employee/restore-employee-list/restore-employee-list.component';
import { RestoreEmployeeListItemComponent } from './restore-employee/restore-employee-list/restore-employee-list-item/restore-employee-list-item.component';
import { DeletedEmployeeResolverService } from '../services/deleted-employee-resolver.service';
import { EmployeeFunFactComponent } from './employee-fun-fact/employee-fun-fact.component';

const routes: Routes = [
  {
    path: 'admin',
    component: EmployeeComponent,
    canActivate: [AuthGaurd],
    resolve: { employees: EmployeeResolverService, jobs: JobResolverService }
  },
  {
    path: 'restore',
    component: RestoreEmployeeComponent,
    canActivate: [AuthGaurd],
    resolve: {
      employees: EmployeeResolverService,
      deletedEmployees: DeletedEmployeeResolverService,
      jobs: JobResolverService
    }
  }
];

@NgModule({
  declarations: [
    EmployeeComponent,
    EmployeeDetailComponent,
    EmployeeAvatarComponent,
    EmployeeDeskComponent,
    RestoreEmployeeComponent,
    RestoreEmployeeDetailComponent,
    RestoreEmployeeListComponent,
    RestoreEmployeeListItemComponent,
    EmployeeFunFactComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    MatCheckboxModule,
    MatFormFieldModule,
    MatInputModule,
    MatSnackBarModule,
    MatDialogModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatAutocompleteModule,
    ImageCropperModule,

    FuseSharedModule,
    FuseConfirmDialogModule,

    SharedModule
  ]
})
export class EmployeeAdminModule {}
