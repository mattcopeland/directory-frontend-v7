import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';

import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { Job } from 'app/main/apps/admin/models/job.model';

@Component({
  selector: 'restore-employee',
  templateUrl: './restore-employee.component.html',
  styleUrls: ['./restore-employee.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RestoreEmployeeComponent implements OnInit, OnDestroy {
  searchInput: FormControl;
  employees: Employee[];
  deletedEmployees: Employee[];
  filteredEmployees: Employee[];
  selectedEmployee: Employee;
  jobs: Job[];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {TodoService} _todoService
   * @param {EmployeeService} _employeeService
   */
  constructor(
    private _route: ActivatedRoute,
    private _employeeService: EmployeeService
  ) {
    // Set the defaults
    this.searchInput = new FormControl('');

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.employees = this._route.snapshot.data['employees'];
    this.deletedEmployees = this._route.snapshot.data['deletedEmployees'];
    this.jobs = this._route.snapshot.data['jobs'];

    this._employeeService.changeSelectedEmployee(null);

    // Subscribe to changes in the search input
    this.searchInput.valueChanges
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe(searchText => {
        this.filteredEmployees = this.filterEmployees(searchText);
      });

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Search for a string in the specified properties of each employee
   *
   * @param {string} searchText
   */
  filterEmployees(searchText): Employee[] | null {
    // If there are not characters in the search box don't show any employees
    if (searchText.length === 0) {
      return null;
    }
    // Must find a match for each word in the search
    const searchWords = searchText.split(' ');
    // Which fields to search
    const fieldsToSearch = ['firstName', 'lastName', 'nickname'];

    searchText = searchText.toLowerCase();

    return this.deletedEmployees.filter(itemObj => {
      return this.searchInObj(itemObj, searchWords, fieldsToSearch);
    });
  }

  /**
   * Search for words in desired fields
   *
   * @param {Employee} itemObj
   * @param {Array} searchWords
   * @param {Array} fieldsToSearch
   */
  searchInObj(itemObj, searchWords, fieldsToSearch): boolean {
    for (let i = 0; i < searchWords.length; i++) {
      let foundWord = false;
      // Search for ith word in all fields
      for (let j = 0; j < fieldsToSearch.length; j++) {
        const value = itemObj[fieldsToSearch[j]];
        // if this word is found we can move onto the next word
        // if this word is not found in this field check the next field
        if (value.toLowerCase().startsWith(searchWords[i])) {
          foundWord = true;
        }
      }
      // if we didn't find this word in any fields don't add this employee to the list of filtered employees
      if (!foundWord) {
        return false;
      }
    }
    // We found each word in atleast 1 field, so include this employee
    return true;
  }

  /**
   * Select an employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee | null): void {
    this._employeeService.changeSelectedEmployee(employee);
  }
}
