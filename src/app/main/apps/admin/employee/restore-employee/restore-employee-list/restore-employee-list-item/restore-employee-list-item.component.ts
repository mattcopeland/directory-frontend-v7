import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

import { Employee } from 'app/main/apps/employee/models/employee.model';

@Component({
  selector: 'restore-employee-list-item',
  templateUrl: './restore-employee-list-item.component.html',
  styleUrls: ['./restore-employee-list-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RestoreEmployeeListItemComponent implements OnInit {
  info: string;
  @Input()
  employee: Employee;

  constructor() {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    // Set the initial values
    this.employee = new Employee(this.employee);
  }
}
