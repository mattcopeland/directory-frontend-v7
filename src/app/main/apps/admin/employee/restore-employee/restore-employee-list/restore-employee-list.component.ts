import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  OnDestroy,
  ViewChild,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { fuseAnimations } from '@fuse/animations';

import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';
@Component({
  selector: 'restore-employee-list',
  templateUrl: './restore-employee-list.component.html',
  styleUrls: ['./restore-employee-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RestoreEmployeeListComponent
  implements OnInit, OnDestroy, OnChanges {
  employees: Employee[];
  selectedEmployee: Employee;

  @Input()
  filteredEmployees: Employee[];
  @Input()
  searchInput: string;

  @ViewChild(FusePerfectScrollbarDirective)
  directiveScroll: FusePerfectScrollbarDirective;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   */
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  /**
   * On chnages
   *
   * @param {SimpleChanges} changes
   */
  ngOnChanges(changes: SimpleChanges): void {
    setTimeout(() => {
      this.scrollToTop();
    });
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * View the employee detail of the selected employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee): void {
    this._employeeService.changeSelectedEmployee(employee);
    // Add the eid to the url for refreshing
    // this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }

  /**
   * Scroll to the bottom
   *
   * @param {number} speed
   */
  scrollToTop(speed?: number): void {
    speed = speed || 400;
    if (this.directiveScroll) {
      this.directiveScroll.update();

      setTimeout(() => {
        this.directiveScroll.scrollToTop(0, speed);
      });
    }
  }
}
