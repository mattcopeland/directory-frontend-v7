import {
  Component,
  OnInit,
  ViewEncapsulation,
  OnDestroy,
  Input,
  ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatDialogRef, MatDialog } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';
import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import { takeUntil, startWith, map } from 'rxjs/operators';
import { Subject, Observable } from 'rxjs';
import * as _ from 'lodash';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { AppUtils } from 'app/core/utils';
import { Job } from 'app/main/apps/admin/models/job.model';
import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { UserService } from 'app/core/services/user.service';

@Component({
  selector: 'restore-employee-detail',
  templateUrl: './restore-employee-detail.component.html',
  styleUrls: ['./restore-employee-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RestoreEmployeeDetailComponent implements OnInit, OnDestroy {
  employeeForm: FormGroup;
  selectedEmployee: Employee | null;
  manager: Employee | null;
  directReports: Employee[] | null;
  departments: string[];
  jobTitles: string[];
  jobClassifications = ['Peak', 'Lead', 'Senior', 'Associate', 'Intern'];
  filteredEmployees: Observable<Employee[]>;
  confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
  adminUser: boolean;

  @Input()
  jobs: Job[];

  @ViewChild(FusePerfectScrollbarDirective)
  directiveScroll: FusePerfectScrollbarDirective;

  // Private
  private _unsubscribeAll: Subject<any>;
  private employees: Employee[];
  private deletedEmployees: Employee[];
  // filter for manager autocomplete
  private _filter(employeeName: string): Employee[] {
    const filterValue = employeeName.toLowerCase();
    return this.employees.filter(
      e => e.displayName.toLowerCase().indexOf(filterValue) === 0
    );
  }

  /**
   * Constructor
   *
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   * @param {FormBuilder} _formBuilder
   * @param {MatSnackBar} snackBar
   * @param {AdminDataAccessService} _adminDataAccessService
   */
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _adminDataAccessService: AdminDataAccessService,
    public _userService: UserService,
    private _matDialog: MatDialog
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.initializeEmployeeForm();

    this._adminDataAccessService
      .getDeletedEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(deletedEmployees => {
        this.deletedEmployees = deletedEmployees;
      });

    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => {
        this.employees = employees;
      });

    this.departments = AppUtils.getUniquePropertyValuesFromObjectArray(
      this.jobs,
      'department'
    ).sort();
    this.jobTitles = AppUtils.getUniquePropertyValuesFromObjectArray(
      this.jobs,
      'jobTitle'
    ).sort();

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
        if (this.selectedEmployee && this.employees) {
          this.filterJobTitlesByDepartment(this.selectedEmployee.department);
          this.employeeForm.patchValue({
            eid: selectedEmployee.eid,
            nickname: selectedEmployee.nickname,
            email: selectedEmployee.email,
            firstName: selectedEmployee.firstName,
            lastName: selectedEmployee.lastName,
            phone: selectedEmployee.phone,
            ext: selectedEmployee.ext,
            team: selectedEmployee.team,
            department: selectedEmployee.department,
            jobTitle: selectedEmployee.jobTitle,
            jobClassification: selectedEmployee.jobClassification,
            hireDate: selectedEmployee.hireDate,
            birthdate: selectedEmployee.birthdate,
            manager: ''
          });
          setTimeout(() => {
            this.scrollToTop();
          });
        } else {
          this.initializeEmployeeForm();
        }
      });

    // Auto complete for manager
    this.filteredEmployees = this.employeeForm.controls[
      'manager'
    ].valueChanges.pipe(
      startWith<string | Employee>(''),
      map(value => (typeof value === 'string' ? value : value.displayName)),
      map(name => (name ? this._filter(name) : this.employees.slice()))
    );
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  initializeEmployeeForm(): void {
    this.employeeForm = this._formBuilder.group({
      eid: [
        { value: '', disabled: !this._userService.isAdmin },
        [Validators.required]
      ],
      nickname: [''],
      email: ['', [Validators.required, Validators.email]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      phone: [''],
      ext: [''],
      team: [{ value: '', disabled: !this._userService.isAdmin }],
      department: [{ value: '', disabled: !this._userService.isAdmin }],
      jobTitle: [{ value: '', disabled: !this._userService.isAdmin }],
      jobClassification: [{ value: '', disabled: !this._userService.isAdmin }],
      hireDate: [{ value: '', disabled: !this._userService.isAdmin }],
      birthdate: [{ value: '', disabled: !this._userService.isAdmin }],
      manager: [{ value: '', disabled: !this._userService.isAdmin }]
    });

    // Auto complete for manager
    this.filteredEmployees = this.employeeForm.controls[
      'manager'
    ].valueChanges.pipe(
      startWith<string | Employee>(''),
      map(value => (typeof value === 'string' ? value : value.displayName)),
      map(name => (name ? this._filter(name) : this.employees.slice()))
    );
  }

  /**
   * Get all the job tiltes in a specified department
   *
   * @param {string} department
   */
  filterJobTitlesByDepartment(department: string): void {
    this.jobTitles = [];
    const departmentJobs = this.jobs.filter(job => {
      return job.department.toLowerCase() === department.toLowerCase();
    });
    departmentJobs.forEach(job => {
      this.jobTitles.push(job.jobTitle);
    });
  }

  /**
   * Used to display the manager's display name in the auto complete box
   *
   * @param {Employee} employee
   */
  displayManagerName(employee?: Employee): string | undefined {
    return employee ? employee.displayName : undefined;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Restore the employee
   */
  restoreEmployee(): void {
    const employeeData = this.employeeForm.value;
    employeeData.displayName =
      (this.employeeForm.value.nickname || this.employeeForm.value.firstName) +
      ' ' +
      this.employeeForm.value.lastName;
    // Only admins will be allowed to update the manager
    if (this.employeeForm.value.manager) {
      employeeData.mid = this.employeeForm.value.manager.eid;
      delete employeeData.manager;
    }
    // Restore the employee
    employeeData.deleted = false;

    // Pass along the collection id to update user info
    employeeData._id = this.selectedEmployee._id;

    // Remove the restored employee from the list of deleted employees
    const deletedEmployeeIndex = this.deletedEmployees.findIndex(e => {
      return e.eid === this.selectedEmployee.eid;
    });
    const deletedEmployee = this.deletedEmployees.splice(
      deletedEmployeeIndex,
      1
    );

    // Update this employee in the list of all employees
    const restoredEmployee = Object.assign(deletedEmployee, employeeData);
    this.employees.push(restoredEmployee);
    this._employeeService.changeSelectedEmployee(restoredEmployee);

    this._adminDataAccessService
      .updateEmployee(employeeData)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        data => {
          // Show a snackbar success
          this._snackBar.open('Employee updated successfully', null, {
            duration: 3000,
            panelClass: 'success'
          });
          this._router.navigate([
            '/apps/admin/employee/admin',
            { eid: employeeData.eid }
          ]);
        },
        // Show a snackbar error
        (response: any) => {
          this._snackBar.open(response.error.message, null, {
            duration: 3000,
            panelClass: 'error'
          });
        }
      );
  }

  /**
   * View the employee detail of the selected employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee): void {
    this._employeeService.changeSelectedEmployee(employee);
    setTimeout(() => {
      this.scrollToTop();
    });
  }

  /**
   * Scroll to the top
   *
   * @param {number} speed
   */
  scrollToTop(speed?: number): void {
    speed = speed || 400;
    if (this.directiveScroll) {
      this.directiveScroll.update();

      setTimeout(() => {
        this.directiveScroll.scrollToTop(0, speed);
      });
    }
  }
}
