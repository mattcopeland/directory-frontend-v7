import {
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
  ViewChild,
  ElementRef
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormControl } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { Job } from 'app/main/apps/admin/models/job.model';
import { UserService } from 'app/core/services/user.service';

@Component({
  selector: 'employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class EmployeeComponent implements OnInit, OnDestroy {
  searchInput: FormControl;
  employees: Employee[];
  filteredEmployees: Employee[];
  selectedEmployee: Employee;
  filter: Object;
  jobs: Job[];

  @ViewChild('searchText')
  searchTextRef: ElementRef;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {TodoService} _todoService
   * @param {EmployeeService} _employeeService
   */
  constructor(
    private _route: ActivatedRoute,
    private _employeeService: EmployeeService,
    private _userService: UserService
  ) {
    // Set the defaults
    this.searchInput = new FormControl('');

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.employees = this._route.snapshot.data['employees'];
    this.jobs = this._route.snapshot.data['jobs'];

    const eid = +this._route.snapshot.params['eid'];

    // Get the employee based on the eid from the url param
    // or from the employee service
    if (eid) {
      const e = this.employees.find(employee => employee.eid === eid);
      if (e) {
        this.selectEmployee(e);
      } else {
        this.selectedEmployee = this._employeeService.selectedEmployee;
      }
    } else {
      this.selectEmployee(
        this.employees.find(
          employee => employee.eid === +this._userService.currentUser.eid
        )
      );
    }

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Select an employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee | null): void {
    this._employeeService.changeSelectedEmployee(employee);
  }
}
