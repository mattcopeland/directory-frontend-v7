import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';

@Component({
  selector: 'employee-desk',
  templateUrl: './employee-desk.component.html',
  styleUrls: ['./employee-desk.component.scss'],
  animations: fuseAnimations
})
export class EmployeeDeskComponent implements OnInit, OnDestroy {
  selectedEmployee: Employee;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {AdminDataAccessService} _adminDataAccessService
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   */
  constructor(
    private _adminDataAccessService: AdminDataAccessService,
    private _employeeService: EmployeeService,
    private _router: Router
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Upload the updated avatar
   */
  updateDesk(): void {
    this._router.navigate([
      '/apps/admin/office',
      { eid: this.selectedEmployee.eid }
    ]);
  }
}
