import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { MatSnackBar } from '@angular/material';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { AdminDataAccessService } from 'app/main/apps/admin/services/admin-data-access.service';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'employee-fun-fact',
  templateUrl: './employee-fun-fact.component.html',
  styleUrls: ['./employee-fun-fact.component.scss'],
  animations: fuseAnimations
})
export class EmployeeFunFactComponent implements OnInit, OnDestroy {
  funFactForm: FormGroup;
  selectedEmployee: Employee;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   *
   * @param {AdminDataAccessService} _adminDataAccessService
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   * @param {UserService} _userService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _adminDataAccessService: AdminDataAccessService,
    private _employeeService: EmployeeService,
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;

        const funFact = this.selectedEmployee
          ? this.selectedEmployee.funFact
          : '';
        this.funFactForm = this._formBuilder.group({
          funFact: [funFact, [Validators.required]]
        });
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Save the employee's fun fact
   */
  saveFunFact(): void {
    const funFactData = {
      funFact: this.funFactForm.value.funFact,
      eid: this.selectedEmployee.eid
    };
    this._adminDataAccessService
      .updateFunFact(funFactData)
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        data => {
          // Set the new fun fact for the employee
          this.selectedEmployee.funFact = funFactData.funFact;
          this._employeeService.changeSelectedEmployee(this.selectedEmployee);
          this._router.navigate([
            '/apps/employee',
            { eid: this.selectedEmployee.eid }
          ]);
        },
        // Show a snackbar error
        (response: any) => {
          this._snackBar.open(response.error.message, null, {
            duration: 3000,
            panelClass: 'error'
          });
        }
      );
  }
}
