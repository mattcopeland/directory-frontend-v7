import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { Subject } from 'rxjs';
import { startOfDay, isSameDay, isSameMonth } from 'date-fns';
import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarMonthViewDay
} from 'angular-calendar';

import { FuseConfirmDialogComponent } from '@fuse/components/confirm-dialog/confirm-dialog.component';
import { fuseAnimations } from '@fuse/animations';

import { CalendarService } from 'app/main/apps/calendar/calendar.service';
import {
    CalendarEventModel,
    ICalendarEvent
} from 'app/main/apps/calendar/event.model';
import { CalendarEventFormDialogComponent } from 'app/main/apps/calendar/event-form/event-form.component';
import { subscribeOn } from 'rxjs/operators';
import { UserService } from 'app/core/services/user.service';
import { takeUntil } from 'rxjs/internal/operators';

@Component({
    selector: 'calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CalendarComponent implements OnInit, OnDestroy {
    actions: CalendarEventAction[];
    activeDayIsOpen: boolean;
    confirmDialogRef: MatDialogRef<FuseConfirmDialogComponent>;
    dialogRef: any;
    events: ICalendarEvent[];
    refresh: Subject<any> = new Subject();
    selectedDay: any;
    view: string;
    viewDate: Date;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _matDialog: MatDialog,
        private _calendarService: CalendarService,
        private _router: Router,
        public userService: UserService
    ) {
        // Set the defaults
        this.view = 'month';
        this.viewDate = new Date();
        this.activeDayIsOpen = true;
        this.selectedDay = { date: startOfDay(new Date()) };

        this.actions = [
            {
                label: '<i class="material-icons s-16">edit</i>',
                onClick: ({ event }: { event: ICalendarEvent }): void => {
                    this.editEvent('edit', event);
                }
            },
            {
                label: '<i class="material-icons s-16">delete</i>',
                onClick: ({ event }: { event: ICalendarEvent }): void => {
                    this.deleteEvent(event);
                }
            }
        ];

        /**
         * Get events from service/server
         */
        this.setEvents();

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        /**
         * Watch re-render-refresh for updating db
         */
        this.refresh.subscribe(updateDB => {
            if (updateDB) {
                this._calendarService
                    .getEvents()
                    .then(data => this.setEvents());
            }
        });

        this._calendarService.onEventsUpdated
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(events => {
                this.setEvents();
                this.refresh.next();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Set events
     */
    setEvents(): void {
        this.events = this._calendarService.events.map(item => {
            if (
                this.userService.isAuthenticated &&
                (this.userService.isAdmin ||
                    this.userService.currentUser['id'] === item.owner)
            ) {
                item.actions = this.actions;
            }
            return new CalendarEventModel(item);
        });
    }

    /**
     * Before View Renderer
     *
     * @param {any} header
     * @param {any} body
     */
    beforeMonthViewRender({ header, body }): void {
        /**
         * Get the selected day
         */
        const _selectedDay = body.find(_day => {
            return _day.date.getTime() === this.selectedDay.date.getTime();
        });

        if (_selectedDay) {
            /**
             * Set selectedday style
             * @type {string}
             */
            _selectedDay.cssClass = 'cal-selected';
        }
    }

    /**
     * Day clicked
     *
     * @param {MonthViewDay} day
     */
    dayClicked(day: CalendarMonthViewDay): void {
        const date: Date = day.date;
        const events: CalendarEvent[] = day.events;

        if (isSameMonth(date, this.viewDate)) {
            if (
                (isSameDay(this.viewDate, date) &&
                    this.activeDayIsOpen === true) ||
                events.length === 0
            ) {
                this.activeDayIsOpen = false;
            } else {
                this.activeDayIsOpen = true;
                this.viewDate = date;
            }
        }
        this.selectedDay = day;
        this.refresh.next();
    }

    /**
     * Event times changed
     * Event dropped or resized
     *
     * @param {CalendarEvent} event
     * @param {Date} newStart
     * @param {Date} newEnd
     */
    eventTimesChanged({
        event,
        newStart,
        newEnd
    }: CalendarEventTimesChangedEvent): void {
        event.start = newStart;
        event.end = newEnd;
        this.refresh.next(true);
    }

    /**
     * Delete Event
     *
     * @param {ICalendarEvent} event
     */
    deleteEvent(event: ICalendarEvent): void {
        this.confirmDialogRef = this._matDialog.open(
            FuseConfirmDialogComponent,
            {
                disableClose: false
            }
        );

        this.confirmDialogRef.componentInstance.confirmMessage =
            'Are you sure you want to delete?';

        this.confirmDialogRef
            .afterClosed()
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(result => {
                if (result) {
                    const eventIndex = this.events.indexOf(event);
                    this.events.splice(eventIndex, 1);
                    this._calendarService.deleteEvent(event).subscribe();
                    this.refresh.next(true);
                }
                this.confirmDialogRef = null;
            });
    }

    /**
     * View Event
     *
     * @param {CalendarEvent} event
     */
    viewEvent(event: ICalendarEvent): void {
        this.dialogRef = this._matDialog.open(
            CalendarEventFormDialogComponent,
            {
                panelClass: 'event-form-dialog',
                data: {
                    event: event,
                    action: 'view',
                    multiDay: !isSameDay(event.start, event.end)
                }
            }
        );
    }

    /**
     * Edit Event
     *
     * @param {string} action
     * @param {CalendarEvent} event
     */
    editEvent(action: string, event: ICalendarEvent): void {
        const eventIndex = this.events.indexOf(event);
        this.dialogRef = this._matDialog.open(
            CalendarEventFormDialogComponent,
            {
                panelClass: 'event-form-dialog',
                data: {
                    event: event,
                    action: action
                }
            }
        );

        this.dialogRef
            .afterClosed()
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(response => {
                if (!response) {
                    return;
                }
                const actionType: string = response[0];
                const formData: FormGroup = response[1];
                switch (actionType) {
                    /**
                     * Save
                     */
                    case 'save':
                        this.events[eventIndex] = Object.assign(
                            this.events[eventIndex],
                            formData.getRawValue()
                        );
                        this._calendarService
                            .updateEvent(this.events[eventIndex])
                            .subscribe();
                        this.refresh.next(true);

                        break;
                    /**
                     * Delete
                     */
                    case 'delete':
                        this.deleteEvent(event);

                        break;
                }
            });
    }

    /**
     * Add Event
     */
    addEvent(): void {
        if (!this.userService.isAuthenticated) {
            this.userService.redirectUrl = '/apps/calendar';
            this._router.navigate(['/pages/auth/login']);
        } else {
            this.dialogRef = this._matDialog.open(
                CalendarEventFormDialogComponent,
                {
                    panelClass: 'event-form-dialog',
                    data: {
                        action: 'new',
                        date: this.selectedDay.date
                    }
                }
            );
            this.dialogRef
                .afterClosed()
                .pipe(takeUntil(this._unsubscribeAll))
                .subscribe((response: FormGroup) => {
                    if (!response) {
                        return;
                    }
                    const newEvent = response.getRawValue();
                    this._calendarService
                        .createEvent(newEvent)
                        .pipe(takeUntil(this._unsubscribeAll))
                        .subscribe();

                    newEvent.actions = this.actions;
                    this.events.push(newEvent);
                    this.refresh.next(true);
                });
        }
    }
}
