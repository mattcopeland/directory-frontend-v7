import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot
} from '@angular/router';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class CalendarService implements Resolve<any> {
  events: any;
  onEventsUpdated: Subject<any>;

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {
    // Set the defaults
    this.onEventsUpdated = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Resolver
   *
   * @param {ActivatedRouteSnapshot} route
   * @param {RouterStateSnapshot} state
   * @returns {Observable<any> | Promise<any> | any}
   */
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    return new Promise((resolve, reject) => {
      Promise.all([this.getEvents()]).then(([events]: [any]) => {
        resolve();
      }, reject);
    });
  }

  /**
   * Get events
   *
   * @returns {Promise<any>}
   */
  getEvents(): Promise<any> {
    return new Promise((resolve, reject) => {
      this._httpClient.get('api/calendar/events').subscribe((data: any) => {
        this.events = data;
        this.onEventsUpdated.next(this.events);
        resolve(this.events);
      }, reject);
    });
  }

  createEvent(event): Observable<any> {
    return this._httpClient.post('/api/calendar/events/create', event);
  }

  updateEvent(event): Observable<any> {
    return this._httpClient.put('/api/calendar/events/update', event);
  }

  deleteEvent(event): Observable<any> {
    return this._httpClient.put('/api/calendar/events/delete', event);
  }
}
