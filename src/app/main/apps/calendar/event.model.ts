import { CalendarEventAction } from 'angular-calendar';
import { startOfDay, endOfDay } from 'date-fns';
import { CalendarEvent } from 'calendar-utils';

export interface ICalendarEvent extends CalendarEvent {
  owner?: string;
}

export class CalendarEventModel {
  id: string;
  owner?: string;
  start: Date;
  end?: Date;
  title: string;
  color: {
    primary: string;
    secondary: string;
  };
  actions?: CalendarEventAction[];
  allDay?: boolean;
  cssClass?: string;
  resizable?: {
    beforeStart?: boolean;
    afterEnd?: boolean;
  };
  draggable?: boolean;
  meta?: {
    location: string;
    notes: string;
  };

  /**
   * Constructor
   *
   * @param data
   */
  constructor(data?) {
    data = data || {};
    this.id = data._id || 0;
    this.owner = data.owner || '';
    this.start = new Date(data.start) || startOfDay(new Date());
    this.end = new Date(data.end) || endOfDay(new Date());
    this.title = data.title || '';
    this.color = {
      primary: (data.color && data.color.primary) || '#1e90ff',
      secondary: (data.color && data.color.secondary) || '#D1E8FF'
    };
    this.draggable = data.draggable || false;
    this.resizable = {
      beforeStart: (data.resizable && data.resizable.beforeStart) || false,
      afterEnd: (data.resizable && data.resizable.afterEnd) || false
    };
    this.actions = data.actions || [];
    this.allDay = data.allDay || false;
    this.cssClass = data.cssClass || '';
    this.meta = {
      location: (data.meta && data.meta.location) || '',
      notes: (data.meta && data.meta.notes) || ''
    };
  }
}
