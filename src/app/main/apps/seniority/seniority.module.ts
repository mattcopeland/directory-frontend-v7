import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  MatIconModule,
  MatListModule,
  MatButtonModule,
  MatSliderModule,
  MatCardModule,
  MatRippleModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { EmployeeResolverService } from '../employee/services/employee-resolver.service';
import { SeniorityComponent } from 'app/main/apps/seniority/seniority.component';
import { SidebarComponent } from './sidebar/sidebar.component';

const routes: Routes = [
  {
    path: '',
    component: SeniorityComponent,
    resolve: { employees: EmployeeResolverService }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatSliderModule,
    MatCardModule,
    MatRippleModule,

    FuseSharedModule,
    FuseSidebarModule
  ],
  declarations: [SeniorityComponent, SidebarComponent]
})
export class SeniorityModule {}
