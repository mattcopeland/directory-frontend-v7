import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

@Component({
  selector: 'seniority-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input()
  yearsOfService: number[];

  /**
   * Constructor
   *
   * @param {Router} _router
   * @param {FuseSidebarService} _fuseSidebarService
   */
  constructor(
    private _router: Router,
    private _fuseSidebarService: FuseSidebarService
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Select a year to view
   *
   * @param {number} year
   */
  selectYear(year: number): void {
    this._router.navigate(['/apps/seniority', { years: year }]);
    this.toggleSidebar('seniority-sidebar');
  }

  /**
   * Toggle sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }
}
