import { Injectable } from '@angular/core';

import * as _ from 'lodash';

import { Employee } from 'app/main/apps/employee/models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class SeniorityService {
  private sortedEmployees: any[] = [];

  constructor() {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Group and order employees based on seniority
   *
   */
  getSortedEmployees(employees: Employee[]): Array<Employee[]> {
    // Use a cached version if it exists
    // Don't want to do all this iterating more than once
    if (this.sortedEmployees.length) {
      return this.sortedEmployees;
    }

    employees.forEach(employee => {
      // If an employee doesn't have any years of service then skip them
      if (employee.yearsOfService !== undefined) {
        // If this year of service array has not been created yet, then create it
        if (!this.sortedEmployees[employee.yearsOfService]) {
          this.sortedEmployees[employee.yearsOfService] = [];
        }
        this.sortedEmployees[employee.yearsOfService].push(employee);
      }
    });
    // Now that the employees are grouped based on years of service
    // order them by hire date in each group
    this.sortedEmployees.forEach((employeeGroup, index) => {
      this.sortedEmployees[index] = _.sortBy(employeeGroup, 'hireDate');
    });
    return this.sortedEmployees;
  }
}
