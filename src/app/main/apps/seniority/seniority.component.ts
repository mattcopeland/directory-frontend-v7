import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject } from 'rxjs';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { fuseAnimations } from '@fuse/animations';

import { SeniorityService } from 'app/main/apps/seniority/services/seniority.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'seniority',
  templateUrl: './seniority.component.html',
  styleUrls: ['./seniority.component.scss'],
  animations: fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class SeniorityComponent implements OnInit, OnDestroy {
  employees: Employee[];
  sortedEmployees: Array<Employee[]>;
  selectedYear: number;
  yearsOfService: number[] = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseSidebarService} _fuseSidebarService
   * @param {ActivatedRoute} _route
   * @param {Router} _router
   * @param {SeniorityService} _seniorityService
   */
  constructor(
    private _fuseSidebarService: FuseSidebarService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _seniorityService: SeniorityService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Remove excluded employees from search results
    this.employees = this._route.snapshot.data['employees'].filter(employee => {
      return !employee.exclude;
    });

    if (this._route.snapshot.params.years) {
      this.selectedYear = this._route.snapshot.params.years;
    }

    // Subscribe to changes of route params
    this._route.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(params => {
        if (params['years']) {
          this.selectYear(params['years']);
        }
      });

    this.sortedEmployees = this._seniorityService.getSortedEmployees(
      this.employees
    );

    // Get all the years of service to display in the sidebar
    for (const key of this.sortedEmployees.keys()) {
      if (this.sortedEmployees[key]) {
        this.yearsOfService.push(key);
      }
    }
    // Don't allow display of the less than 1 year people
    this.yearsOfService.shift();
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Select a year to view
   *
   * @param {number} year
   */
  selectYear(year: number): void {
    this.selectedYear = year;
    this._router.navigate(['/apps/seniority', { years: year }]);
  }

  /**
   * Navigate to the selected employee's profile
   *
   * @param {Employee} employee
   */
  viewEmployeeProfile(employee: Employee): void {
    this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }

  /**
   * Toggle sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }
}
