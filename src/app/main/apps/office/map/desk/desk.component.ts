import { Component, Input, ViewEncapsulation } from '@angular/core';

import { Desk } from 'app/main/apps/office/models/desk.model';

@Component({
  selector: 'desk',
  templateUrl: './desk.component.html',
  styleUrls: ['./desk.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DeskComponent {
  @Input()
  desk: Desk;
  @Input()
  deskDisplay: string;

  constructor() {}
}
