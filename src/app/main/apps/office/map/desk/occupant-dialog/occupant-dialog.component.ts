import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { Desk } from 'app/main/apps/office/models/desk.model';

@Component({
  selector: 'occupant-dialog',
  templateUrl: './occupant-dialog.component.html',
  styleUrls: ['./occupant-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OccupantDialogComponent {
  desk: Desk;
  occupant: Employee;
  dialogTitle: string;

  /**
   * Constructor
   *
   * @param {MatDialogRef<OccupantDialogComponent>} matDialogRef
   * @param _data
   * @param {Router} _router
   */
  constructor(
    public matDialogRef: MatDialogRef<OccupantDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any,
    private _router: Router
  ) {
    this.desk = _data.desk;
    this.occupant = _data.desk.occupant;
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Navigate to an employee's profile
   *
   * @param {number} eid
   */
  viewProfile(eid: number): void {
    this.matDialogRef.close();
    this._router.navigate(['/apps/employee', { eid: eid }]);
  }
}
