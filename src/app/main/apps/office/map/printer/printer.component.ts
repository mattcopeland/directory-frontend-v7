import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';

import { Printer } from 'app/main/apps/office/models/printer.model';

@Component({
  selector: 'printer',
  templateUrl: './printer.component.html',
  styleUrls: ['./printer.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PrinterComponent {
  @Input()
  printer: Printer;

  constructor() {}
}
