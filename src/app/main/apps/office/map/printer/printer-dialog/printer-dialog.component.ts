import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';

import { Printer } from 'app/main/apps/office/models/printer.model';

@Component({
  selector: 'printer-dialog',
  templateUrl: './printer-dialog.component.html',
  styleUrls: ['./printer-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class PrinterDialogComponent {
  printer: Printer;

  /**
   * Constructor
   *
   * @param {MatDialogRef<PrinterDialogComponent>} matDialogRef
   * @param _data
   */
  constructor(
    public matDialogRef: MatDialogRef<PrinterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any
  ) {
    this.printer = _data.printer;
  }
}
