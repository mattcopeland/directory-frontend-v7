import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

import { fuseAnimations } from '@fuse/animations';

import { Room } from 'app/main/apps/office/models/room.model';

@Component({
  selector: 'room-dialog',
  templateUrl: './room-dialog.component.html',
  styleUrls: ['./room-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class RoomDialogComponent {
  room: Room;
  dialogTitle: string;

  /**
   * Constructor
   *
   * @param {MatDialogRef<RoomDialogComponent>} matDialogRef
   * @param _data
   */
  constructor(
    public matDialogRef: MatDialogRef<RoomDialogComponent>,
    @Inject(MAT_DIALOG_DATA) private _data: any
  ) {
    this.room = _data.room;
  }
}
