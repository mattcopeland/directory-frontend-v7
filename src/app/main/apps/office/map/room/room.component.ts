import { Component, Input, ViewEncapsulation } from '@angular/core';

import { Room } from 'app/main/apps/office/models/room.model';

@Component({
  selector: 'room',
  templateUrl: './room.component.html',
  styleUrls: ['./room.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RoomComponent {
  @Input()
  room: Room;

  constructor() {}
}
