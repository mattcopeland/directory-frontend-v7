import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'marker',
  templateUrl: './marker.component.html',
  styleUrls: ['./marker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MarkerComponent {
  constructor() {}
}
