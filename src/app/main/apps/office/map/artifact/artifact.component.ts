import { Component, Input, ViewEncapsulation } from '@angular/core';

import { Artifact } from 'app/main/apps/office/models/artifact.model';

@Component({
  selector: 'artifact',
  templateUrl: './artifact.component.html',
  styleUrls: ['./artifact.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ArtifactComponent {
  @Input()
  artifact: Artifact;

  constructor() {}
}
