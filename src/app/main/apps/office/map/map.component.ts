import {
  Component,
  OnInit,
  Input,
  OnDestroy,
  ViewEncapsulation
} from '@angular/core';

import { MatDialog, MatSnackBar } from '@angular/material';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { OfficeService } from 'app/main/apps/office/services/office.service';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { OccupantDialogComponent } from 'app/main/apps/office/map/desk/occupant-dialog/occupant-dialog.component';
import { Desk } from 'app/main/apps/office/models/desk.model';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { Artifact } from 'app/main/apps/office/models/artifact.model';
import { Room } from 'app/main/apps/office/models/room.model';
import { RoomDialogComponent } from 'app/main/apps/office/map/room/room-dialog/room-dialog.component';
import { Printer } from 'app/main/apps/office/models/printer.model';
import { PrinterDialogComponent } from 'app/main/apps/office/map/printer/printer-dialog/printer-dialog.component';

@Component({
  selector: 'map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class MapComponent implements OnInit, OnDestroy {
  // desks: Desk[];
  employees: Employee[];
  artifacts: Artifact[];
  rooms: Room[];
  printers: Printer[];
  hasMappedDesk: Boolean = false;
  hasMappedRoom: Boolean = false;
  dialogRef: any;
  @Input()
  floor: number;
  @Input()
  officeCode: string;
  @Input()
  mappedDesk: Desk;
  @Input()
  mappedRoom: Room;
  @Input()
  desks: Desk[];
  @Input()
  deskDisplay: string;

  get employeesMappedToDesks(): Desk[] {
    return this._officeService.employeesMappedToDesks;
  }

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {OfficeService} _officeService
   * @param {EmployeeService} _employeeService
   * @param {MatDialog} _matDialog
   * @param {MatSnackBar} _snackBar
   */
  constructor(
    private _officeService: OfficeService,
    private _employeeService: EmployeeService,
    private _matDialog: MatDialog,
    private _snackBar: MatSnackBar
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    if (
      this.mappedDesk &&
      this.mappedDesk.officeCode === this.officeCode &&
      this.mappedDesk.floor === +this.floor
    ) {
      this.hasMappedDesk = true;
    } else if (
      this.mappedRoom &&
      this.mappedRoom.officeCode === this.officeCode &&
      this.mappedRoom.floor === +this.floor
    ) {
      this.hasMappedRoom = true;
    }

    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => {
        this.employees = employees;
        this.mapOccupantToDesks(this.desks);
      });

    this._officeService
      .getArtifacts()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(artifacts => {
        this.filterArtifacts(artifacts);
      });

    this._officeService
      .getRooms()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(rooms => {
        this.filterRooms(rooms);
      });

    this._officeService
      .getPrinters()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(printers => {
        this.filterPrinters(printers);
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filter the complete list of desks to only the desks for this location
   *
   * @param {Desk[]} desks
   */
  filterDesks(desks: Desk[]): void {
    this.desks = this.desks.filter(
      desk => desk.floor === +this.floor && desk.officeCode === this.officeCode
    );
  }

  /**
   * Map each employee to the desk that occupy
   *
   * @param {Desk[]} desks
   */
  mapOccupantToDesks(desks: Desk[]): void {
    if (!this.employeesMappedToDesks) {
      this.desks.forEach(desk => {
        desk.occupant = this.employees.find(employee => {
          return (
            desk.seat === employee.seat &&
            desk.officeCode === employee.officeCode
          );
        });
      });
    }
    this._officeService.employeesMappedToDesks = this.desks;
    this.filterDesks(this.desks);
  }

  /**
   * Filter the complete list of artifacts to only the artifacts for this location
   *
   * @param {Artifact[]} artifacts
   */
  filterArtifacts(artifacts: Artifact[]): void {
    this.artifacts = artifacts.filter(
      artifact =>
        artifact.floor === +this.floor &&
        artifact.officeCode === this.officeCode
    );
  }

  /**
   * Filter the complete list of rooms to only the rooms for this location
   *
   * @param {Room[]} artifacts
   */
  filterRooms(rooms: Room[]): void {
    this.rooms = rooms.filter(
      room => room.floor === +this.floor && room.officeCode === this.officeCode
    );
  }

  /**
   * Filter the complete list of printers to only the printers for this location
   *
   * @param {Printer[]} printer
   */
  filterPrinters(printers: Printer[]): void {
    this.printers = printers.filter(
      printer =>
        printer.floor === +this.floor && printer.officeCode === this.officeCode
    );
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Open a dialog for a selected desk
   *
   * @param {Desk} desk
   */
  selectDesk(desk: Desk): void {
    if (desk.occupant) {
      this.dialogRef = this._matDialog.open(OccupantDialogComponent, {
        panelClass: 'occupant-dialog',
        data: {
          desk: desk
        }
      });
    } else {
      // Show a snackbar success
      this._snackBar.open(`No one sits there right now.`, null, {
        duration: 3000,
        panelClass: 'error'
      });
    }
  }

  /**
   * Open a dialog for a selected printer
   *
   * @param {Printer} printer
   */
  selectPrinter(printer: Printer): void {
    this.dialogRef = this._matDialog.open(PrinterDialogComponent, {
      panelClass: 'printer-dialog',
      data: {
        printer: printer
      }
    });
  }

  /**
   * Open a dialog for a selected room
   *
   * @param {Room} room
   */
  selectRoom(room: Room): void {
    this.dialogRef = this._matDialog.open(RoomDialogComponent, {
      panelClass: 'room-dialog',
      data: {
        room: room
      }
    });
  }
}
