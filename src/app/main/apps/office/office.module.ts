import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  MatTabsModule,
  MatDialogModule,
  MatIconModule,
  MatToolbarModule,
  MatButtonModule,
  MatRadioModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { OfficeComponent } from './office.component';
import { MapComponent } from './map/map.component';
import { DeskResolverService } from 'app/main/apps/office/services/desk-resolver.service';
import { DeskComponent } from './map/desk/desk.component';
import { EmployeeResolverService } from 'app/main/apps/employee/services/employee-resolver.service';
import { MarkerComponent } from './map/marker/marker.component';
import { OccupantDialogComponent } from 'app/main/apps/office/map/desk/occupant-dialog/occupant-dialog.component';
import { ArtifactComponent } from './map/artifact/artifact.component';
import { RoomComponent } from './map/room/room.component';
import { PrinterComponent } from './map/printer/printer.component';
import { PrinterDialogComponent } from 'app/main/apps/office/map/printer/printer-dialog/printer-dialog.component';
import { RoomDialogComponent } from './map/room/room-dialog/room-dialog.component';
import { RoomResolverService } from './services/room-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: OfficeComponent,
    resolve: {
      desks: DeskResolverService,
      employees: EmployeeResolverService,
      rooms: RoomResolverService
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTabsModule,
    MatDialogModule,
    MatIconModule,
    MatToolbarModule,
    MatButtonModule,
    MatRadioModule,

    FuseSharedModule
  ],
  declarations: [
    OfficeComponent,
    MapComponent,
    DeskComponent,
    MarkerComponent,
    OccupantDialogComponent,
    ArtifactComponent,
    RoomComponent,
    PrinterComponent,
    PrinterDialogComponent,
    RoomDialogComponent
  ],
  entryComponents: [
    OccupantDialogComponent,
    PrinterDialogComponent,
    RoomDialogComponent
  ]
})
export class OfficeModule {}
