import {
  Component,
  OnInit,
  ViewEncapsulation,
  AfterContentInit,
  ElementRef,
  ViewChild
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { fuseAnimations } from '@fuse/animations';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import { Desk } from 'app/main/apps/office/models/desk.model';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { Room } from './models/room.model';
import { OfficeService } from './services/office.service';

@Component({
  selector: 'office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class OfficeComponent implements OnInit, AfterContentInit {
  desks: Desk[];
  employees: Employee[];
  rooms: Room[];
  maps: string[] = ['lzbuf-6', 'lzbuf-7', 'lzbuf-8', 'lznyc-6'];
  mapIndex: Number = 0;
  mappedDesk: Desk;
  mappedRoom: Room;
  deskDisplay = 'occupant';

  @ViewChild(FusePerfectScrollbarDirective)
  directiveScroll: FusePerfectScrollbarDirective;

  get employeesMappedToDesks(): Desk[] {
    return this._officeService.employeesMappedToDesks;
  }

  constructor(
    private _route: ActivatedRoute,
    private elRef: ElementRef,
    private _officeService: OfficeService
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    this.desks =
      this.employeesMappedToDesks || this._route.snapshot.data['desks'];
    this.employees = this._route.snapshot.data['employees'];

    this.rooms = this._route.snapshot.data['rooms'];

    // Map a desk for a seat from the url
    if (
      this._route.snapshot.params.seat &&
      this._route.snapshot.params.officeCode
    ) {
      const d = this.desks.find(
        desk =>
          desk.seat === this._route.snapshot.params.seat &&
          desk.officeCode === this._route.snapshot.params.officeCode
      );
      if (d) {
        this.mapDesk(d);
      }
    } else if (
      this._route.snapshot.params.room &&
      this._route.snapshot.params.officeCode
    ) {
      const r = this.rooms.find(
        room =>
          room.location === this._route.snapshot.params.room &&
          room.officeCode === this._route.snapshot.params.officeCode
      );
      if (r) {
        this.mapRoom(r);
      }
    }
  }

  // for transcluded content
  ngAfterContentInit(): void {
    setTimeout(() => {
      let element;
      if (this._route.snapshot.params.seat) {
        element = this.elRef.nativeElement.querySelector(
          '#desk-' + this._route.snapshot.params.seat
        );
      } else if (this._route.snapshot.params.room) {
        element = this.elRef.nativeElement.querySelector(
          '#room-' + this._route.snapshot.params.room
        );
      }

      // This doesn't seem to work in Chrome on Windows
      // So I had use the uglier animation below
      if (element && element.offsetTop > 400) {
        this.scrollToY(element.offsetTop, 1);
      }

      // Using this because the above code doesn't work on Chrome on Windows
      //   if (element && element.offsetTop > 400) {
      //     setTimeout(() => {
      //       element.scrollIntoView({
      //         // behavior: 'smooth',
      //         behavior: 'auto', // Would rather use smooth scrolling but it doesn't seem to work reliably in Chrome
      //         block: 'start',
      //         inline: 'nearest'
      //       });
      //     }, 200);
      //   }
    }, 700);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Show a selected desk on the map
   *
   * @param desk
   */
  private mapDesk(desk): void {
    // Select the appropriate tab
    this.mapIndex = this.maps.indexOf(desk.officeCode + '-' + desk.floor);
    // Show the map marker on the desk
    this.mappedDesk = desk;
  }

  /**
   * Show a selected room on the map
   *
   * @param room
   */
  private mapRoom(room): void {
    // Select the appropriate tab
    this.mapIndex = this.maps.indexOf(room.officeCode + '-' + room.floor);
    // Show the map marker on the room
    this.mappedRoom = room;
  }

  /**
   * Scroll to the top
   *
   * @param {number} speed
   */
  scrollToY(y: number, speed?: number): void {
    speed = speed || 400;
    if (this.directiveScroll) {
      this.directiveScroll.update();

      setTimeout(() => {
        this.directiveScroll.scrollToY(y, speed);
      }, 1000);
    }
  }
}
