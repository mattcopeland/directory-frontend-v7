export class Office {
  address1: string;
  address2: number;
  building: string;
  city: string;
  code: string;
  country: string;
  floors: number[];
  postalCode: number;
  state: string;

  /**
   * Constructor
   *
   * @param office
   */
  constructor(office) {
    {
      this.address1 = office.address1;
      this.address2 = office.address2;
      this.building = office.building;
      this.city = office.city;
      this.code = office.code;
      this.country = office.country;
      this.floors = office.floors;
      this.postalCode = office.postalCode;
      this.state = office.state;
    }
  }
}
