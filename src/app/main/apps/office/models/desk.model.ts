import { Employee } from 'app/main/apps/employee/models/employee.model';

export class Desk {
  officeCode: string;
  floor: number;
  seat: string;
  drops: string;
  orientation: string;
  classificaiton: string;
  xpos: number;
  ypos: number;
  height?: number;
  width?: number;
  occupant?: Employee;

  /**
   * Constructor
   *
   * @param desk
   */
  constructor(desk) {
    {
      this.officeCode = desk.officeCode;
      this.floor = desk.floor;
      this.seat = desk.seat;
      this.drops = desk.drops;
      this.orientation = desk.orientation;
      this.classificaiton = desk.classificaiton;
      this.xpos = desk.xpos;
      this.ypos = desk.ypos;
      this.height = desk.height || null;
      this.width = desk.width || null;
      this.occupant = desk.occupant || null;
    }
  }
}
