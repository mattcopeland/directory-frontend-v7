export class Room {
  officeCode: string;
  floor: number;
  location?: string;
  name: string;
  classificaiton: string;
  type: string;
  phone: number;
  ext: number;
  capacity: number;
  xpos: number;
  ypos: number;
  height: number;
  width: number;

  /**
   * Constructor
   *
   * @param room
   */
  constructor(room) {
    {
      this.officeCode = room.officeCode;
      this.floor = room.floor;
      this.location = room.location || '';
      this.name = room.name;
      this.classificaiton = room.classificaiton;
      this.type = room.type;
      this.phone = room.phone;
      this.ext = room.ext;
      this.capacity = room.capacity;
      this.xpos = room.xpos;
      this.ypos = room.ypos;
      this.height = room.height;
      this.width = room.width;
    }
  }
}
