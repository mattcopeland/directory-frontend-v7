export class Artifact {
  officeCode: string;
  floor: number;
  name: string;
  orientation: string;
  classificaiton: string;
  xpos: number;
  ypos: number;
  height: number;
  width: number;

  /**
   * Constructor
   *
   * @param artifact
   */
  constructor(artifact) {
    {
      this.officeCode = artifact.officeCode;
      this.floor = artifact.floor;
      this.name = artifact.name;
      this.orientation = artifact.orientation;
      this.classificaiton = artifact.classificaiton;
      this.xpos = artifact.xpos;
      this.ypos = artifact.ypos;
      this.height = artifact.height;
      this.width = artifact.width;
    }
  }
}
