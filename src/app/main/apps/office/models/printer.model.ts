export class Printer {
  officeCode: string;
  floor: number;
  name: string;
  brand: string;
  model: string;
  orientation: string;
  classification: string;
  xpos: number;
  ypos: number;

  /**
   * Constructor
   *
   * @param printer
   */
  constructor(printer) {
    {
      this.officeCode = printer.officeCode;
      this.floor = printer.floor;
      this.name = printer.name;
      this.brand = printer.brand;
      this.model = printer.brand;
      this.orientation = printer.orientation;
      this.classification = printer.classification;
      this.xpos = printer.xpos;
      this.ypos = printer.ypos;
    }
  }
}
