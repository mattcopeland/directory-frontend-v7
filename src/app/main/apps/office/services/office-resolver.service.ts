import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Observable } from 'rxjs';

import { Office } from 'app/main/apps/office/models/office.model';
import { OfficeService } from 'app/main/apps/office/services/office.service';

@Injectable({
  providedIn: 'root'
})
export class OfficeResolverService implements Resolve<Office[]> {
  constructor(private _officeService: OfficeService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Office[]> {
    return this._officeService.getOffices();
  }
}
