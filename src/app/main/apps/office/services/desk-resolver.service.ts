import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Observable } from 'rxjs';

import { Desk } from 'app/main/apps/office/models/desk.model';
import { OfficeService } from './office.service';

@Injectable({
  providedIn: 'root'
})
export class DeskResolverService implements Resolve<Desk[]> {
  constructor(private _officeService: OfficeService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Desk[]> {
    return this._officeService.getDesks();
  }
}
