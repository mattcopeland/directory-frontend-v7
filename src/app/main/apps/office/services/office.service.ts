import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Office } from 'app/main/apps/office/models/office.model';
import { Desk } from 'app/main/apps/office/models/desk.model';
import { Artifact } from 'app/main/apps/office/models/artifact.model';
import { Room } from 'app/main/apps/office/models/room.model';
import { Printer } from 'app/main/apps/office/models/printer.model';

@Injectable({
  providedIn: 'root'
})
export class OfficeService {
  offices: Office[];
  desks: Desk[];
  employeesMappedToDesks: Desk[];
  artifacts: Artifact[];
  rooms: Room[];
  printers: Printer[];

  private selectedDeskSource = new BehaviorSubject<Desk | null>(null);
  selectedDeskChanges$ = this.selectedDeskSource.asObservable();

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get offices
   *
   */
  getOffices(): Observable<Office[]> {
    return this._httpClient
      .get<Office[]>('/api/offices')
      .pipe(tap(data => (this.offices = data)));
  }

  /**
   * Get desks
   *
   */
  getDesks(): Observable<Desk[]> {
    return this._httpClient
      .get<Desk[]>('/api/desks')
      .pipe(tap(data => (this.desks = data)));
  }

  /**
   * Change selected desk observable
   *
   * @param selectedDesk
   */
  changeSelectedDesk(selectedDesk: Desk | null): void {
    this.selectedDeskSource.next(selectedDesk);
  }

  /**
   * Get artifacts
   *
   */
  getArtifacts(): Observable<Artifact[]> {
    return this._httpClient
      .get<Artifact[]>('/api/artifacts')
      .pipe(tap(data => (this.artifacts = data)));
  }

  /**
   * Get rooms
   *
   */
  getRooms(): Observable<Room[]> {
    return this._httpClient
      .get<Room[]>('/api/rooms')
      .pipe(tap(data => (this.rooms = data)));
  }

  /**
   * Get printers
   *
   */
  getPrinters(): Observable<Printer[]> {
    return this._httpClient
      .get<Printer[]>('/api/printers')
      .pipe(tap(data => (this.printers = data)));
  }
}
