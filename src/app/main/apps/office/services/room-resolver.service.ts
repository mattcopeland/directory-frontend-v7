import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Observable } from 'rxjs';

import { Room } from 'app/main/apps/office/models/room.model';
import { OfficeService } from './office.service';

@Injectable({
  providedIn: 'root'
})
export class RoomResolverService implements Resolve<Room[]> {
  constructor(private _officeService: OfficeService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Room[]> {
    return this._officeService.getRooms();
  }
}
