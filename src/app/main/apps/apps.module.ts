import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
  {
    path: 'calendar',
    loadChildren: './calendar/calendar.module#CalendarModule'
  },
  {
    path: 'employee',
    loadChildren: './employee/employee.module#EmployeeModule'
  },
  {
    path: 'office',
    loadChildren: './office/office.module#OfficeModule'
  },
  {
    path: 'seniority',
    loadChildren: './seniority/seniority.module#SeniorityModule'
  },
  {
    path: 'birthday',
    loadChildren: './birthday/birthday.module#BirthdayModule'
  },
  {
    path: 'committee',
    loadChildren: './committee/committee.module#CommitteeModule'
  },
  {
    path: 'admin',
    loadChildren: './admin/admin.module#AdminModule'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes), FuseSharedModule]
})
export class AppsModule {}
