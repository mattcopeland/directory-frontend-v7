import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';

import { fuseAnimations } from '@fuse/animations';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import * as _ from 'lodash';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { UserService } from 'app/core/services/user.service';
import { CommitteeService } from '../../committee/services/committee.service';
import { SelectedEmployee } from '../models/selected-employee.model';

@Component({
  selector: 'employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class EmployeeDetailComponent implements OnInit, OnDestroy {
  selectedEmployee: SelectedEmployee | null;
  manager: Employee | null;
  directReports: Employee[] | null;

  // Private
  private _unsubscribeAll: Subject<any>;
  private employees: Employee[];

  /**
   * Constructor
   *
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   * @param {UserService} userService
   * @param {CommitteeService} _committeeService
   */
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router,
    public userService: UserService,
    private _committeeService: CommitteeService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => (this.employees = employees));

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
        if (this.selectedEmployee && this.employees) {
          if (this.selectedEmployee.eid === 1396134) {
            this.selectedEmployee.image = 'zoni.png';
          } else if (this.selectedEmployee.eid === 1209608) {
            this.selectedEmployee.image = 'tomk.gif';
          }
          this.getManager(this.selectedEmployee);
          this.getDirectReports(this.selectedEmployee);
          this.selectedEmployee.committees = [];
          this._committeeService.committees.forEach(committee => {
            if (committee.eids.find(eid => eid === this.selectedEmployee.eid)) {
              this.selectedEmployee.committees.push(committee);
            }
          });
        }
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get the manager for an employee
   *
   * @param {Employee} employee
   */
  private getManager(employee: Employee): void {
    this.manager = this.employees.find(e => {
      return e.eid === employee.mid;
    });
  }

  /**
   * Get a list of direct reports for an employee
   *
   * @param {Employee} employee
   */
  private getDirectReports(employee: Employee): void {
    this.directReports = _.sortBy(
      this.employees.filter(e => {
        return e.mid === employee.eid;
      }),
      'lastName'
    );
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * View the employee detail of the selected employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee): void {
    this._employeeService.changeSelectedEmployee(employee);
    // Add the eid to the url for refreshing
    this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }

  /**
   * Shows where a selected employee sits
   *
   * @param {Employee} employee
   */
  mapEmployee(employee: Employee): void {
    if (employee.seat) {
      this._router.navigate([
        '/apps/office',
        { seat: employee.seat, officeCode: employee.officeCode }
      ]);
    }
  }

  /**
   * View the sneiority page for a selected number of years
   *
   * @param {number} years
   */
  viewYearsOfService(years: number): void {
    this._router.navigate(['/apps/seniority', { years: years }]);
  }

  /**
   * Opens an email message with a selected employee
   *
   * @param {string} email
   */
  mailto(email): void {
    const mail = document.createElement('a');
    mail.href = 'mailto:' + email;
    mail.click();
  }

  /**
   * Opens a skype chat session with a selected employee
   *
   * @param {string} email
   */
  chatto(email: string): void {
    const chat = document.createElement('a');
    chat.href = 'sip:' + email;
    chat.click();
  }

  /**
   * Remove the selected employee to displat the search in small screen
   */
  backToList(): void {
    this._employeeService.changeSelectedEmployee(null);
  }
}
