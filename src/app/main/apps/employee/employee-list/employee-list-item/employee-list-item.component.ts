import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

import { Router } from '@angular/router';

import { Employee } from 'app/main/apps/employee/models/employee.model';

@Component({
  selector: 'employee-list-item',
  templateUrl: './employee-list-item.component.html',
  styleUrls: ['./employee-list-item.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EmployeeListItemComponent implements OnInit {
  info: string;
  @Input()
  employee: Employee;

  constructor(private _router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit(): void {
    // Set the initial values
    this.employee = new Employee(this.employee);
  }
}
