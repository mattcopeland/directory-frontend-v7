import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  OnDestroy,
  ViewChild,
  ElementRef
} from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';

import { Subject } from 'rxjs';
import { takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

import { EmployeeService } from '../services/employee.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';

@Component({
  selector: 'employee-list',
  templateUrl: './employee-list.component.html',
  styleUrls: ['./employee-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class EmployeeListComponent implements OnInit, OnDestroy {
  searchInput: FormControl;
  filteredEmployees: Employee[];
  filteredEmployeesBy: Employee[];
  selectedEmployee: Employee;
  checkFilters: Boolean;
  selectedFilters: Object;
  filter: Object;
  activeEmployeeListItem = -1;

  @Input() employees: Employee[];
  @ViewChild('searchText') searchInputRef: ElementRef;
  @ViewChild(FusePerfectScrollbarDirective)
  directiveScroll: FusePerfectScrollbarDirective;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {EmployeeService} _employeeService
   * @param {Router} _router
   */
  constructor(
    private _employeeService: EmployeeService,
    private _router: Router
  ) {
    // Set the defaults
    this.searchInput = new FormControl('');

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedEmployee => {
        this.selectedEmployee = selectedEmployee;
      });

    // Subscribe to changes in the search input
    this.searchInput.valueChanges
      .pipe(
        takeUntil(this._unsubscribeAll),
        debounceTime(300),
        distinctUntilChanged()
      )
      .subscribe(searchText => {
        this.scrollToTop();
        this.filteredEmployeesBy = this.filteredEmployees = this.filterEmployees(
          searchText
        );
        // Apply the filters if any are set
        if (this.checkFilters) {
          this.filteredEmployeesBy = this.filterEmployeesBy(
            this.selectedFilters
          );
        }
      });

    // Subscribe to filter changes
    this._employeeService.selectedFilterChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedFilters => {
        this.scrollToTop();
        // Unset the team filter if they didn't select development department
        if (
          selectedFilters['department'] &&
          selectedFilters['department'] !== 'development'
        ) {
          selectedFilters['team'] = '';
        }
        this.activeEmployeeListItem = -1;
        this.filteredEmployeesBy = this.filterEmployeesBy(selectedFilters);
        this.selectedFilters = selectedFilters;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Search for a string in the specified properties of each employee
   *
   * @param {string} searchText
   */
  filterEmployees(searchText): Employee[] | null {
    this.activeEmployeeListItem = -1;
    // If there are not characters in the search box don't show any employees
    if (searchText.length === 0) {
      return null;
    }
    // You just got rick rolled
    if (searchText.toLowerCase() === 'rick') {
      window.open('https://www.youtube.com/watch?v=oHg5SJYRHA0');
    }
    searchText = searchText.toLowerCase();
    // Must find a match for each word in the search
    const searchWords = searchText.split(' ');
    // Which fields to search
    const fieldsToSearch = ['firstName', 'lastName', 'nickname'];

    return this.employees.filter(itemObj => {
      return this.searchInObj(itemObj, searchWords, fieldsToSearch);
    });
  }

  /**
   * Search for words in desired fields
   *
   * @param {Employee} itemObj
   * @param {Array} searchWords
   * @param {Array} fieldsToSearch
   */
  searchInObj(itemObj, searchWords, fieldsToSearch): boolean {
    for (let i = 0; i < searchWords.length; i++) {
      let foundWord = false;
      // Search for ith word in all fields
      for (let j = 0; j < fieldsToSearch.length; j++) {
        const value = itemObj[fieldsToSearch[j]];
        // if this word is found we can move onto the next word
        // if this word is not found in this field check the next field
        if (value.toLowerCase().startsWith(searchWords[i])) {
          foundWord = true;
        }
      }
      // if we didn't find this word in any fields don't add this employee to the list of filtered employees
      if (!foundWord) {
        return false;
      }
    }
    // We found each word in atleast 1 field, so include this employee
    return true;
  }

  /**
   * Filter the filtered employees by the selected filters
   *
   * @param {Employee[] | null} filters
   */
  filterEmployeesBy(filters): Employee[] | null {
    this.filteredEmployeesBy = [];
    // Only filter down the filteredEmployees list is something is selected from the filters
    this.checkFilters = false;
    for (const i in filters) {
      if (filters[i]) {
        this.checkFilters = true;
        break;
      }
    }
    // If there are selected filters and employees to be filtered
    if (this.checkFilters) {
      // Either filter the list of filtered employees (text filter)
      // or filter the list of filtered employees filtered by other properties
      let employees = this.filteredEmployees || this.employees;
      for (const filter in filters) {
        if (filters.hasOwnProperty(filter)) {
          // Should we use the full list of employees or a filtered list
          if (this.filteredEmployeesBy.length) {
            employees = this.filteredEmployeesBy;
          }
          if (filters[filter]) {
            this.filteredEmployeesBy = employees.filter(employee => {
              return (
                employee[filter].toLowerCase() === filters[filter].toLowerCase()
              );
            });
          }
        }
      }
      // Return the list of filtered employees filtered by other properties
      return this.filteredEmployeesBy;
    } else {
      // Return the full list of filtered employees since we have nothing to filter by
      return this.filteredEmployees;
    }
  }

  /**
   * View the employee detail of the selected employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee, index: number): void {
    this.activeEmployeeListItem = index;
    this.searchInputRef.nativeElement.focus();
    this._employeeService.changeSelectedEmployee(employee);
    // Add the eid to the url for refreshing
    this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }

  /**
   * Track keyup, keydown and enter keys to traverse the list of employees
   *
   * @param event
   */
  onSearchKeydown(event): void {
    if (
      event.key === 'ArrowDown' &&
      this.activeEmployeeListItem < this.filteredEmployeesBy.length - 1
    ) {
      this.activeEmployeeListItem += 1;
    } else if (event.key === 'ArrowUp' && this.activeEmployeeListItem > 0) {
      event.preventDefault();
      this.activeEmployeeListItem -= 1;
    } else if (event.key === 'Enter' && this.activeEmployeeListItem >= 0) {
      this.selectEmployee(
        this.filteredEmployeesBy[this.activeEmployeeListItem],
        this.activeEmployeeListItem
      );
    }
  }

  clearSearch(): void {
    this.searchInput.patchValue('');
  }

  /**
   * Scroll to the bottom
   *
   * @param {number} speed
   */
  scrollToTop(): void {
    if (this.directiveScroll) {
      this.directiveScroll.update();

      setTimeout(() => {
        this.directiveScroll.scrollToTop(0, 0);
      });
    }
  }
}
