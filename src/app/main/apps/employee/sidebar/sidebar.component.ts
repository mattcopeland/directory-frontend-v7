import {
  Component,
  OnInit,
  ViewEncapsulation,
  OnDestroy,
  Input
} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';

import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { AppUtils } from 'app/core/utils';

@Component({
  selector: 'employee-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class SidebarComponent implements OnInit, OnDestroy {
  filterForm: FormGroup;
  departmentFilters: string[];
  teamFilters: string[];
  filters: object = {
    department: '',
    team: ''
  };

  @Input()
  filter: object;

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {EmployeeService} _employeeService
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _employeeService: EmployeeService,
    private _formBuilder: FormBuilder
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this.filterForm = this._formBuilder.group({
      department: [''],
      team: ['']
    });

    // Order each the filter properties alphabetically
    this._employeeService
      .getEmployees()
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(employees => {
        this.departmentFilters = AppUtils.getUniquePropertyValuesFromObjectArray(
          employees,
          'department'
        ).sort();
        this.teamFilters = AppUtils.getUniquePropertyValuesFromObjectArray(
          employees,
          'team'
        ).sort();
      });

    // Subscribe to filter changes
    // This will remember filters that were selected previously
    this._employeeService.selectedFilterChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(selectedFilters => {
        // Set the filters if a value was passed into the component
        if (selectedFilters) {
          // Clear out the selected employee
          // Object.keys(selectedFilters).forEach(key => {
          //   if (!selectedFilters[key]) {
          //     this._employeeService.changeSelectedEmployee(null);
          //   }
          // });

          if (selectedFilters['department']) {
            // Reset and disable the teams filter if Development department is not selected
            if (selectedFilters['department'].toLowerCase() !== 'development') {
              this.filterForm.patchValue({
                team: null
              });
              selectedFilters['team'] = '';
              this.filterForm.controls.team.disable();
            } else {
              this.filterForm.controls.team.enable();
            }
            this.filterForm.patchValue({
              department: selectedFilters['department'].toLowerCase()
            });
          } else if (selectedFilters['team']) {
            this.filterForm.patchValue({
              team: selectedFilters['team'].toLowerCase()
            });
          } else {
            this.filterForm.controls.team.enable();
          }
        }
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Filter the (already filtered) employees by a chosen property
   *
   * @param {string} value
   * @param {string} prop
   */
  filterEmployeesBy(value: string, prop: string): void {
    this.filters[prop] = value;
    this._employeeService.changeSelectedFilter(this.filters);
  }
}
