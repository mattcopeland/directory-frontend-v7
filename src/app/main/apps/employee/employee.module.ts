import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import {
  MatIconModule,
  MatSelectModule,
  MatButtonModule,
  MatToolbarModule,
  MatCardModule,
  MatRippleModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { SharedModule } from 'app/shared/shared.module';

import { EmployeeResolverService } from 'app/main/apps/employee/services/employee-resolver.service';
import { EmployeeComponent } from 'app/main/apps/employee/employee.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeListItemComponent } from './employee-list/employee-list-item/employee-list-item.component';
import { EmployeeDetailComponent } from './employee-detail/employee-detail.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { CommitteeResolverService } from '../committee/services/committee-resolver.service';

const routes: Routes = [
  {
    path: '',
    component: EmployeeComponent,
    resolve: {
      employees: EmployeeResolverService,
      committees: CommitteeResolverService
    }
  }
];

@NgModule({
  declarations: [
    EmployeeComponent,
    EmployeeListComponent,
    EmployeeListItemComponent,
    EmployeeDetailComponent,
    SidebarComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),

    MatIconModule,
    MatSelectModule,
    MatButtonModule,
    MatToolbarModule,
    MatCardModule,
    MatRippleModule,

    FuseSharedModule,
    FuseSidebarModule,

    SharedModule
  ],
  providers: []
})
export class EmployeeModule {}
