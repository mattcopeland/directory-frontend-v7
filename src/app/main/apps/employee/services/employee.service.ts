import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable, of, Subject, BehaviorSubject } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { SelectedEmployee } from 'app/main/apps/employee/models/selected-employee.model';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
  employees: Employee[];
  selectedEmployee: SelectedEmployee | null;

  private selectedEmployeeSource = new BehaviorSubject<Employee | null>(
    this.selectedEmployee
  );
  selectedEmployeeChanges$ = this.selectedEmployeeSource.asObservable();

  private selectedFiltersSource = new BehaviorSubject<Object>({});
  selectedFilterChanges$ = this.selectedFiltersSource.asObservable();

  /**
   * Constructor
   *
   * @param {HttpClient} _httpClient
   */
  constructor(private _httpClient: HttpClient) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get employees
   *
   */
  getEmployees(): Observable<Employee[]> {
    return this._httpClient.get<Employee[]>('/api/employees').pipe(
      tap(employees => {
        this.employees = employees;
      })
    );
  }

  /**
   * Get featured employee
   *
   */
  getFeaturedEmployee(): Observable<any[]> {
    return this._httpClient.get<any[]>('/api/employees/featured');
  }

  /**
   * Change selected employee observable
   *
   * @param selectedEmployee
   */
  changeSelectedEmployee(selectedEmployee: SelectedEmployee | null): void {
    this.selectedEmployee = selectedEmployee;
    this.selectedEmployeeSource.next(selectedEmployee);
  }

  /**
   * Change selected filters observable
   *
   * @param selectedFilters
   */
  changeSelectedFilter(selectedFilters: Object): void {
    this.selectedFiltersSource.next(selectedFilters);
  }

  /**
   * Select an employee from their eid
   *
   * @param {number} eid
   */
  selectEmployeeByEid(eid: number): SelectedEmployee {
    const selectedEmployee = this.employees.find(
      employee => employee.eid === eid
    );
    this.changeSelectedEmployee(selectedEmployee);
    return selectedEmployee;
  }
}
