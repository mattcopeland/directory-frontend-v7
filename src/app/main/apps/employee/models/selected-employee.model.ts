import { Employee } from './employee.model';
import { Committee } from '../../committee/models/committee.model';

export class SelectedEmployee extends Employee {
  committees?: Committee[];
}
