import { Office } from 'app/main/apps/office/models/office.model';

export class Employee {
  _id: string;
  eid: number;
  firstName: string;
  lastName: string;
  nickname: string;
  displayName: string;
  birthdate?: Date;
  birthday?: number;
  hireDate: Date;
  yearsOfService: number;
  email: string;
  phone: number;
  ext: number;
  department: string;
  jobTitle: string;
  jobClassification: string;
  team: string;
  office: Office;
  officeCode: string;
  officeCitySate: string;
  floor: number;
  seat: string;
  image: string;
  exclude: boolean;
  mid: number;
  deleted: boolean;
  funFact: string;

  /**
   * Constructor
   *
   * @param employee
   */
  constructor(employee) {
    {
      this._id = employee._id;
      this.eid = employee.eid;
      this.firstName = employee.firstName;
      this.lastName = employee.lastName;
      this.nickname = employee.nickname || '';
      this.displayName = employee.displayName;
      this.birthdate = employee.birthdate || null;
      this.hireDate = employee.hireDate;
      this.yearsOfService = employee.yearsOfService;
      this.email = employee.email;
      this.phone = employee.phone;
      this.ext = employee.ext;
      this.department = employee.department;
      this.jobTitle = employee.jobTitle;
      this.jobClassification = employee.jobClassification;
      this.team = employee.team;
      this.office = employee.office;
      this.officeCode = employee.officeCode;
      this.officeCitySate = employee.officeCitySate;
      this.floor = employee.floor;
      this.seat = employee.seat;
      this.image = employee.image || 'profile.png';
      this.exclude = employee.exclude;
      this.mid = employee.mid;
      this.deleted = employee.deleted;
      this.funFact = employee.funFact;
    }
  }
}
