import {
  Component,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
  QueryList,
  ViewChildren,
  AfterViewInit
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { FormControl } from '@angular/forms';

import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { fuseAnimations } from '@fuse/animations';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

import { Employee } from 'app/main/apps/employee/models/employee.model';
import { EmployeeService } from 'app/main/apps/employee/services/employee.service';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';

@Component({
  selector: 'employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class EmployeeComponent implements OnInit, OnDestroy, AfterViewInit {
  searchInput: FormControl;
  employees: Employee[];
  filteredEmployees: Employee[];
  filteredEmployeesBy: Employee[];
  selectedEmployee: Employee;
  checkFilters: Boolean;
  selectedFilters: Object;
  filter: Object;

  @ViewChildren(FusePerfectScrollbarDirective)
  private _fusePerfectScrollbarDirectives: QueryList<
    FusePerfectScrollbarDirective
  >;

  // Private
  private _employeeDetailScrollbar: FusePerfectScrollbarDirective;
  private _employeeListScrollbar: FusePerfectScrollbarDirective;
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseSidebarService} _fuseSidebarService
   * @param {TodoService} _todoService
   * @param {EmployeeService} _employeeService
   */
  constructor(
    private _fuseSidebarService: FuseSidebarService,
    private _route: ActivatedRoute,
    private _employeeService: EmployeeService
  ) {
    // Set the defaults
    this.searchInput = new FormControl('');

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Remove excluded employees from search results
    this.employees = this._route.snapshot.data['employees'].filter(employee => {
      return !employee.exclude;
    });

    this._route.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(params => {
        const e = this.employees.find(
          employee => employee.eid === +params['eid']
        );
        if (e) {
          this.selectEmployee(e);
        } else {
          this.selectedEmployee = this._employeeService.selectedEmployee;
        }
      });

    if (this._route.snapshot.params.department) {
      this.filter = { department: this._route.snapshot.params.department };
      // Display the employees in a department specified from the url
      this._employeeService.changeSelectedFilter({
        department: this._route.snapshot.params.department
      });
    } else if (this._route.snapshot.params.team) {
      this.filter = { team: this._route.snapshot.params.team };
      // Display the employees on a team specified from the url
      this._employeeService.changeSelectedFilter({
        team: this._route.snapshot.params.team
      });
    }

    // Subscribe to selected employee changes
    this._employeeService.selectedEmployeeChanges$
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(
        selectedEmployee => (this.selectedEmployee = selectedEmployee)
      );
  }

  /**
   * After view init
   */
  ngAfterViewInit(): void {
    this._employeeDetailScrollbar = this._fusePerfectScrollbarDirectives.find(
      directive => {
        return directive.elementRef.nativeElement.id === 'employee-detail';
      }
    );
    this._employeeListScrollbar = this._fusePerfectScrollbarDirectives.find(
      directive => {
        return directive.elementRef.nativeElement.id === 'employee-list';
      }
    );
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Scroll elements to the top
   *
   * @param {FusePerfectScrollbarDirective} ref
   */
  private _scrollToTop(ref: FusePerfectScrollbarDirective): void {
    setTimeout(() => {
      if (ref) {
        ref.update();

        setTimeout(() => {
          ref.scrollToTop(0);
        });
      }
    });
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Select an employee
   *
   * @param {Employee} employee
   */
  selectEmployee(employee: Employee | null): void {
    this._employeeService.changeSelectedEmployee(employee);
    this._scrollToTop(this._employeeDetailScrollbar);
  }

  /**
   * Toggle the sidebar
   *
   * @param {string} name
   */
  toggleSidebar(name: string): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }
}
