import { Injectable } from '@angular/core';
import {
  Resolve,
  ActivatedRouteSnapshot,
  RouterStateSnapshot
} from '@angular/router';

import { Observable } from 'rxjs';

import { Committee } from 'app/main/apps/committee/models/committee.model';
import { CommitteeService } from 'app/main/apps/committee/services/committee.service';

@Injectable({
  providedIn: 'root'
})
export class CommitteeResolverService implements Resolve<Committee[]> {
  constructor(private _committeeService: CommitteeService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<Committee[]> {
    return this._committeeService.getCommittees();
  }
}
