import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { Committee } from '../models/committee.model';

@Injectable({
  providedIn: 'root'
})
export class CommitteeService {
  committees: Committee[];
  selectedCommittee: string | null;

  constructor(private _httpClient: HttpClient) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Get committees
   *
   */
  getCommittees(): Observable<Committee[]> {
    return this._httpClient
      .get<Committee[]>('/api/committees')
      .pipe(tap(data => (this.committees = data)));
  }
}
