import { Component, Input } from '@angular/core';

import { fuseAnimations } from '@fuse/animations';

import { Committee } from '../models/committee.model';
import { Employee } from '../../employee/models/employee.model';
import { Router } from '@angular/router';

@Component({
  selector: 'committee-content',
  templateUrl: './committee-content.component.html',
  styleUrls: ['./committee-content.component.scss'],
  animations: fuseAnimations
})
export class CommitteeContentComponent {
  @Input()
  committee: Committee;

  /**
   * Constructor
   *
   * @param {Router} _router
   */
  constructor(private _router: Router) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Navigate to the selected employee's profile
   *
   * @param {Employee} employee
   */
  viewEmployeeProfile(employee: Employee): void {
    this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }
}
