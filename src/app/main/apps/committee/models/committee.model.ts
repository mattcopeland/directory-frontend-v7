import { Employee } from 'app/main/apps/employee/models/employee.model';

export class Committee {
  code: string;
  name: string;
  description: string;
  eids: Number[];
  members: Employee[];

  /**
   * Constructor
   *
   * @param committee
   */
  constructor(committee) {
    {
      this.code = committee.code;
      this.name = committee.name;
      this.description = committee.description;
      this.eids = committee.eids;
      this.members = committee.members;
    }
  }
}
