import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material';

import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { fuseAnimations } from '@fuse/animations';

import { Employee } from '../employee/models/employee.model';
import { Committee } from './models/committee.model';

@Component({
  selector: 'committee',
  templateUrl: './committee.component.html',
  styleUrls: ['./committee.component.scss'],
  animations: fuseAnimations
})
export class CommitteeComponent implements OnInit, OnDestroy {
  committees: Committee[];
  employees: Employee[];
  selectedCommittee: Committee;
  selectedCommitteeIndex: number;
  tabs: string[] = [];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {ActivatedRoute} _route
   * @param {Router} _router
   */
  constructor(private _route: ActivatedRoute, private _router: Router) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On Init
   */
  ngOnInit(): void {
    this.committees = this._route.snapshot.data['committees'];
    this.employees = this._route.snapshot.data['employees'];

    // Subscribe to changes of route params
    this._route.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(params => {
        if (params['code']) {
          this.selectCommittee(params['code']);
        }
      });

    // Populate all the members from the eids
    this.committees.forEach(committee => {
      this.tabs.push(committee.code);
      committee.members = [];
      committee.eids.forEach(eid => {
        const member = this.employees.find(employee => employee.eid === eid);
        // Check to make sure this is a active employee
        if (member && !member.deleted) {
          committee.members.push(member);
        }
      });
    });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public Methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Update the selected committee and nd route
   *
   * @param {string} code
   */
  selectCommittee(code: string): void {
    this.selectedCommittee = this.committees.find(
      committee => committee.code === code
    );
    this._router.navigate(['/apps/committee', { code: code }]);
    // Select the appropriate tab
    this.selectedCommitteeIndex = this.tabs.indexOf(code);
  }

  /**
   * Update the route when the committee is changed from the tab
   *
   * @param {MatTabChangeEvent} event
   */
  tabChanged(event: MatTabChangeEvent): void {
    this._router.navigate([
      '/apps/committee',
      { code: this.tabs[event.index] }
    ]);
  }
}
