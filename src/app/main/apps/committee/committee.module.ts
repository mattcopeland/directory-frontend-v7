import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  MatButtonModule,
  MatIconModule,
  MatTabsModule,
  MatRippleModule,
  MatCardModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';

import { CommitteeComponent } from './committee.component';
import { EmployeeResolverService } from '../employee/services/employee-resolver.service';
import { CommitteeResolverService } from './services/committee-resolver.service';
import { CommitteeContentComponent } from './committee-content/committee-content.component';

const routes: Routes = [
  {
    path: '',
    component: CommitteeComponent,
    resolve: {
      employees: EmployeeResolverService,
      committees: CommitteeResolverService
    }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatIconModule,
    MatTabsModule,
    MatRippleModule,
    MatCardModule,

    FuseSharedModule
  ],
  declarations: [CommitteeComponent, CommitteeContentComponent]
})
export class CommitteeModule {}
