import { Injectable } from '@angular/core';

import * as _ from 'lodash';
import * as moment from 'moment';

import { Employee } from 'app/main/apps/employee/models/employee.model';

@Injectable({
  providedIn: 'root'
})
export class BirthdayService {
  private sortedEmployees: any[] = [];

  constructor() {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Group and order employees based on seniority
   *
   */
  getSortedEmployees(employees: Employee[]): Array<Employee[]> {
    // Use a cached version if it exists
    // Don't want to do all this iterating more than once
    if (this.sortedEmployees.length) {
      return this.sortedEmployees;
    }

    employees.forEach(employee => {
      // If an employee doesn't have any years of service then skip them
      if (employee.birthdate) {
        const monthIndex = +moment(employee.birthdate).format('M');
        // monthIndex = monthIndex - 1;
        employee.birthday = moment(employee.birthdate).date();
        // If this month array has not been created yet, then create it
        if (!this.sortedEmployees[monthIndex]) {
          this.sortedEmployees[monthIndex] = [];
        }
        this.sortedEmployees[monthIndex].push(employee);
      }
    });
    // Sort the employees in each month by the day of the month
    let i = 0;
    this.sortedEmployees.forEach(monthEmployees => {
      this.sortedEmployees[i] = _.sortBy(monthEmployees, 'birthday');
      i++;
    });

    return this.sortedEmployees;
  }
}
