import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

@Component({
  selector: 'birthday-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  @Input()
  months: string[];

  /**
   * Constructor
   *
   * @param {Router} _router
   * @param {FuseSidebarService} _fuseSidebarService
   */
  constructor(
    private _router: Router,
    private _fuseSidebarService: FuseSidebarService
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {}

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Select a month to view
   *
   * @param {number} month
   */
  selectMonth(month: number): void {
    this._router.navigate(['/apps/birthday', { month: month }]);
    this.toggleSidebar('birthday-sidebar');
  }

  /**
   * Toggle sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }
}
