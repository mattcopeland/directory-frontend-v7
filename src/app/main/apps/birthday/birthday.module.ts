import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {
  MatIconModule,
  MatListModule,
  MatButtonModule,
  MatCardModule,
  MatRippleModule
} from '@angular/material';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { EmployeeResolverService } from '../employee/services/employee-resolver.service';
import { BirthdayComponent } from 'app/main/apps/birthday/birthday.component';
import { SidebarComponent } from './sidebar/sidebar.component';

const routes: Routes = [
  {
    path: '',
    component: BirthdayComponent,
    resolve: { employees: EmployeeResolverService }
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatIconModule,
    MatListModule,
    MatButtonModule,
    MatCardModule,
    MatRippleModule,

    FuseSharedModule,
    FuseSidebarModule
  ],
  declarations: [BirthdayComponent, SidebarComponent]
})
export class BirthdayModule {}
