import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { fuseAnimations } from '@fuse/animations';

import { BirthdayService } from 'app/main/apps/birthday/services/birthday.service';
import { Employee } from 'app/main/apps/employee/models/employee.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'birthday',
  templateUrl: './birthday.component.html',
  styleUrls: ['./birthday.component.scss'],
  animations: fuseAnimations
})
export class BirthdayComponent implements OnInit, OnDestroy {
  employees: Employee[];
  sortedEmployees: Array<Employee[]>;
  selectedMonth: number;
  months: string[] = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   * @param {FuseSidebarService} _fuseSidebarService
   * @param {ActivatedRoute} _route
   * @param {Router} _router
   * @param {BirthdayService} _birthdayService
   */
  constructor(
    private _fuseSidebarService: FuseSidebarService,
    private _route: ActivatedRoute,
    private _router: Router,
    private _birthdayService: BirthdayService
  ) {
    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Remove excluded employees from search results
    this.employees = this._route.snapshot.data['employees'].filter(employee => {
      return !employee.exclude;
    });

    if (this._route.snapshot.params.month) {
      this.selectedMonth = +this._route.snapshot.params.month;
    }

    // Subscribe to changes of route params
    this._route.params
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(params => {
        if (params['month']) {
          this.selectMonth(params['month']);
        }
      });

    this.sortedEmployees = this._birthdayService.getSortedEmployees(
      this.employees
    );
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  /**
   * Select a month to view
   *
   * @param {number} month
   */

  selectMonth(month: number): void {
    this.selectedMonth = month;
    this._router.navigate(['/apps/birthday', { month: month }]);
  }

  /**
   * Navigate to the selected employee's profile
   *
   * @param {Employee} employee
   */
  viewEmployeeProfile(employee: Employee): void {
    this._router.navigate(['/apps/employee', { eid: employee.eid }]);
  }

  /**
   * Toggle sidebar
   *
   * @param name
   */
  toggleSidebar(name): void {
    this._fuseSidebarService.getSidebar(name).toggleOpen();
  }
}
