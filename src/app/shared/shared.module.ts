import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhoneNumberPipe } from 'app/shared/pipes/phone-number.pipe';
import { SafePipe } from 'app/shared/pipes/safe.pipe';

@NgModule({
  imports: [CommonModule],
  declarations: [PhoneNumberPipe, SafePipe],
  exports: [PhoneNumberPipe, SafePipe]
})
export class SharedModule {}
