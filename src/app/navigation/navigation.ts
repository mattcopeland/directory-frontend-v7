import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
  {
    id: 'home',
    title: 'Home',
    type: 'item',
    icon: 'home',
    exactMatch: true,
    url: '/'
  },
  {
    id: 'areas',
    title: 'Areas',
    type: 'group',
    children: [
      {
        id: 'employees',
        title: 'Employees',
        type: 'item',
        icon: 'person',
        url: '/apps/employee'
      },
      {
        id: 'offices',
        title: 'Offices',
        type: 'item',
        icon: 'domain',
        url: '/apps/office'
      },
      {
        id: 'calendar',
        title: 'Calendar',
        type: 'item',
        icon: 'today',
        url: '/apps/calendar'
      },
      {
        id: 'seniority',
        title: 'Seniority',
        type: 'item',
        mdiIcon: 'certificate',
        url: '/apps/seniority'
      },
      {
        id: 'birthday',
        title: 'Birthdays',
        type: 'item',
        icon: 'cake',
        url: '/apps/birthday'
      },
      {
        id: 'committe',
        title: 'Committees',
        type: 'item',
        icon: 'stars',
        url: '/apps/committee'
      },
      {
        id: 'admin',
        title: 'Admin',
        type: 'collapsable',
        icon: 'edit',
        hidden: true,
        children: [
          {
            id: 'admin-employees',
            title: 'Edit Employee',
            type: 'item',
            hiddenShortcut: true,
            url: '/apps/admin/employee/admin'
          },
          {
            id: 'restore-employees',
            title: 'Restore Employee',
            type: 'item',
            hiddenShortcut: true,
            url: '/apps/admin/employee/restore'
          }
        ]
      }
    ]
  }
];
